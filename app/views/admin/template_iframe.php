<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<link type="text/css" rel="stylesheet" href="<?=base_url('css/admin.css?v1');?>" />

<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style>
body{
    padding:20px;
}
.container{
    width:100% !important;
}
</style>
</head>
<body>
<?php
$g=$this->db->get_where('price_agri',array('id'=>$this->uri->segment(3)))->row();
?>
    <h5><?=$g->name;?></h5>
	<div style='height:20px;'></div>  
    <div style="min-height:500px;">

    <?php  echo $output;  ?>

    </div>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>DIT :: Admin</title>
<?php 
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<link type="text/css" rel="stylesheet" href="<?=base_url('css/admin.css?v1');?>" />

<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
</head>
<body>

<header>
<div class="container">
  <div class="row">
   <div class="col-sm-12">
   <a href="<?=site_url('admin_dit');?>" class="logo">
        <img src="<?=base_url('img/logo.svg');?>" alt="DIT" width="100">
    </a>

<div class="btn-group pull-right btn-web">
  <button type="button" class="btn btn-info"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ตั้งค่า</button>
  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?=site_url('admin_dit/users');?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> ผู้ดูแลระบบ</a></li>
    <li><a href="<?=site_url('admin_dit/setting');?>"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> ตั้งค่าเว็บทั่วไป</a></li>
    <li><a href="<?=site_url('admin_dit/social');?>"><span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span> Social network</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="<?=site_url('login/logout');?>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> ออกจากระบบ</a></li>
  </ul>
</div>


   <a href="<?=site_url();?>" target="_blank" class="btn btn-warning pull-right btn-web"> <span class="glyphicon glyphicon-globe" aria-hidden="true"></span> เข้าสู่เว็บ</a>
   


</div>
<div class="clearfix"></div>
   <div class="col-sm-12">
       <ul class="menu-main">
         <li class="<?=$this->uri->segment(2)=="slide_home"?"active":"";?>"><a href="#">หน้าหลัก</a>
            <ul class="dropdown">
               <li><a href="<?=site_url('admin_dit/slide_home');?>">Slide Home</a></li>
            </ul>
         </li>
         <li class="<?=$this->uri->segment(2)=="about"?"active":"";?>"><a href="<?=site_url('admin_dit/about');?>">เกี่ยวกับเรา</a></li>
         <li class="<?=$this->uri->segment(2)=="personnel"?"active":"";?> "><a href="<?=site_url('admin_dit/personnel');?>">บุคลากร</a></li>
         <li class="<?=$this->uri->segment(2)=="contact"||$this->uri->segment(2)=="contact_section"||$this->uri->segment(2)=="contact_department"?"active":"";?> "><a href="<?=site_url('admin_dit/contact');?>">ติตต่อเรา</a>
             <ul class="dropdown">
               <li><a href="<?=site_url('admin_dit/contact_section');?>">หน่วยงานที่ติดต่อ</a></li>
            </ul>
        </li>
         <li class="<?=$this->uri->segment(2)=="complaint"?"active":"";?> "><a href="<?=site_url('admin_dit/complaint');?>">แจ้งปัญหา</a></li>
         <li class="<?=$this->uri->segment(2)=="price_agri"?"active":"";?> "><a href="<?=site_url('admin_dit/price_agri');?>">ราคาเกษตร</a></li>
         <li class="<?=$this->uri->segment(2)=="document"?"active":"";?> "><a href="<?=site_url('admin_dit/document');?>">เอกสารเผยแพร่</a></li>
         <li class="<?=$this->uri->segment(2)=="news"?"active":"";?> "><a href="<?=site_url('admin_dit/news');?>">ข่าวสาร</a></li>
         <li class="<?=$this->uri->segment(2)=="microsite"?"active":"";?> "><a href="<?=site_url('admin_dit/microsite');?>">***ส่วนงานหลัก</a></li>
         <li class="<?=$this->uri->segment(2)=="link_relate" || $this->uri->segment(2)=="link_web"?"active":"";?>"><a href="#">เมนูอื่นๆ</a>
            <ul class="dropdown">
               <li><a href="<?=site_url('admin_dit/link_relate');?>">เว็บไซต์ที่เกี่ยวข้อง</a></li>
               <li><a href="<?=site_url('admin_dit/link_web');?>">เชื่อมโยงไปยังเว็บไซต์</a></li>
            </ul>
         </li>
       </ul>
   </div>
  </div>
</div>
</header>

	<div style='height:20px;'></div>  
    <div>

    <?php if($this->uri->segment(2)!=""): echo $output; ?>
    
  
  <?php else:?>

<div class="i-main">
<div class="box-in">
<div class="row">
        <div class="col-md-8">

        <?php 
        $ro = $this->db->order_by('id','desc')->limit(10)->get('user_log')->result();
        ?>
        <h4>ผู้เข้าระบบล่าสุด</h4>
         <hr>
        <?php if($ro!=false): ?>
        <table class="table table-striped i-log"> <thead>
         <tr> <th>#</th> <th>Username</th> <th>Ip</th> <th>User Agent</th> <th>Date Time</th></tr> 
         
         </thead> 
         <tbody> 
          <?php $ii=1;foreach($ro as $r): ?>
          <tr> <th scope="row"><?=$ii++;?></th> <td><?=$r->username;?></td> <td><?=$r->ip;?></td> <td><?=$r->user_agent;?></td><td><?=$r->date_time;?></td> </tr>
             <?php endforeach;?>
           </tbody> 
           </table>
        <?php endif; ?>
        </div>
        <div class="col-md-4">
        <a href="<?=site_url('admin_dit/microsite');?>" type="button" class="btn btn-primary btn-lg btn-block">ส่วนงานหลัก</a>
        <a href="<?=site_url('admin_dit/intro');?>" type="button" class="btn btn-warning btn-lg btn-block">INTRO สำหรับงานเทศกาล</a>

        </div>
      </div>
</div>
</div>
  <?php endif; ?>

    </div>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>

</body>
</html>

<?php
$deviceId = "DEVICEID";

$username = 'DIT_WEB';
$userpwd = 'ditws2018';
$url = 'https://agri-dit.geniustree.io/ws/price';

$params = array(
  "grant_type" => "password",
  "username" => "DIT_WEB",
  "password" => "ditws2018"
);

$access_token = getAccessToken($url, $params, $userpwd);
$locationInfo = getLocation($deviceId, $access_token);

print_r($locationInfo);

function getAccessToken($url, $params, $userpwd) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, true);
  curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HEADER, 'Content-Type: application/x-www-form-urlencoded;');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $postData = "";
  foreach($params as $k => $v) {
      
    $postData .= $k.
    '='.urlencode($v).
    '&';
  }
  $postData = rtrim($postData, '&');

  curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

  $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  $result = curl_exec($ch);
  curl_close($ch);

  $response = json_decode($result, true);
  $access_token = $response['access_token'];

  return $access_token;
}

function getLocation($deviceId, $access_token) {
  $locationUrl = "URL";
  $ch = curl_init($locationUrl);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$access_token));
  $result = curl_exec($ch);
  curl_close($ch);

  return $result;
} ?>
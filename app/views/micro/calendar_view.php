<?php
$mi = $this->db->get_where('microsite',array('micro_id'=>$id))->row();
?>
<section class="h-page lazy" data-src="<?=base_url('img/bg-h1.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
        <img src="<?=base_url('img/calendar-i.png');?>" class="float-right">
      </div>
    </div>
  </div>
</section>

<section class="page-about bg-cf">
<div class="grid-container bg-w" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1>ปฎิทินกิจกรรม <div class="back"> << ย้อนกลับ</div></h1>
         <h6>หน่วยงาน : <?=$mi->micro_name;?></h6>
      </div>
    </div>
<div class="grid-x">
<div class="cell small-12">
         <hr/>
</div>
</div>
  </div>
</section>


<?php
if($this->uri->segment(4)=="" ||  $this->uri->segment(5) == ""):
$wantee = date('m').'/'.date('Y');
else:
$wantee = $this->uri->segment(5).'/'.$this->uri->segment(4);
endif;
$this->db->select('*');
$this->db->from('calendar_micro');
$this->db->where("DATE_FORMAT(date,'%m/%Y')", $wantee);
$this->db->where("micro_id", $id);
$ww = $this->db->get()->result();

//echo date('m');
?>


<section class="news-all bg-cf">
<div class="grid-container bg-w" >
    <div class="grid-x grid-margin-x grid-padding-x">
    <div class="cell small-12">
<?php 
if($this->uri->segment(4)=="" ||  $this->uri->segment(5) == ""):
  $year = date('Y');
  $month = date('m');
  else:
  $year = $this->uri->segment(4);
  $month = $this->uri->segment(5);
  endif;
$cal_events = array();


if($ww!=false):
foreach ($ww as $e)
{
    if ( date_format(date_create($e->date),"Y") == $year && date_format(date_create($e->date),"m") == $month)
    {       
  $day = date_format( date_create($e->date),"d");
  
  //just made up examples below

  $cal_events[$day] = $e->link; 
    }
}
endif;


echo $this->calendar->generate($this->uri->segment(4), $this->uri->segment(5),$cal_events);?>
      </div>

      <div class="cell small-12">
        <div class="box-list-activity" style="margin-top:0">
          <h4><?
$thaiweek=array("วันอาทิตย์","วันจันทร์","วันอังคาร","วันพุธ","วันพฤหัส","วันศุกร์","วันเสาร์");
$thaimonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");


if($this->uri->segment(4)=="" ||  $this->uri->segment(5) == ""):
  echo "สรุปกิจกรรมเดือน ",$thaimonth[date(" m ")-1] ,date(" Y ")+543;;
  else:
  echo "สรุปกิจกรรมเดือน ",$thaimonth[$this->uri->segment(5)-1] ,$this->uri->segment(4)+543;
  endif;



?> </h4>

<div class="">

  <?php if($ww!=false): ?>
  <ul>
   <?php foreach($ww as $w): ?>

   <li><a href="<?=$w->link;?>"><b><?php
   $date=date_create($w->date);
   echo date_format($date,"d").' '.$thaimonth[date_format($date,"m")-1];
   
   ?></b><br/><?=$w->subject;?></a></li>

   <?php endforeach; ?>

   </ul>

  <?php else: ?>
<br/>
<div class="text-center">
--- ไม่พบกิจกรรมในเดือนนี้ ---
</div>
<br/>
  <?php endif;?>

</div>
        </div>
      </div>



    </div>
    <br/><br/>
  </div>
</section>

<?=$this->load->view('link_web_micro');?>
<?php
//slide
$ge = $this->db->order_by('order_number','asc')->get_where('slide_micro',array('micro_id'=>$id,'status !='=>'close'));
$nSlide= $ge->num_rows();
$g  = $ge->result();

// microsite
$n = $this->db->get_where('microsite',array('micro_id'=>$id))->row();

// ข่าว
$nn = $this->db->order_by('id','desc')->limit(2)->get_where('news_micro',array('micro_id'=>$id,'status !='=>'close'));
$num_news = $nn->num_rows();
$ne = $nn->result();


// ข้อมูลเผยแพร่
$do  = $this->db->get_where('doc_micro',array('micro_id'=>$id,'status !='=>'close'));
$doo = $do->num_rows();
$doc = $do->result();
?>
<section class="slide-h">
  <div class="orbit" role="region" data-auto-play="true" aria-label="Favorite Space Pictures" data-orbit>
    <div class="orbit-wrapper">

    <?php if($nSlide > 1): ?>
      <div class="orbit-controls">
        <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
        <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
      </div>
    <?php endif; ?>

      <?php if($g!=false): ?>
      <ul class="orbit-container">
        <?php $b=0; foreach($g as $r): ?>
        <li class="<?=$b++==0?'is-active':'';?> orbit-slide">
          <a href="<?=$r->slide_micro_link;?>" class="orbit-figure">
          <img class="orbit-image" src="<?=base_url('img/micro/slide/'.$r->slide_micro_picture);?>" alt="Space">
          </a>
        </li>
        <?php endforeach; ?>
      </ul>

    </div>
    <nav class="orbit-bullets">
    <?php $i=0; foreach($g as $r):  ?>
      <button class="<?=$i==0?'is-active':'';?>" data-slide="<?=$i++;?>"></button>
      <?php endforeach; ?>

    </nav>
    <?php endif; ?>
  </div>
</section>

<section class="price-agri bg-cf price-main-box">
  <div class="grid-container bg-w">
    <div class="grid-x">
      <div class="cell small-12 text-center">
        <h3><b><?=$n->micro_name;?></b></h3>
        <hr/>
        <br/>
      </div>
    </div>

<?php 

$sizebox = "";
if($num_news>0) {
    
    if($doo>1 && $doo <3){
      $grid= "small-up-3 medium-up-5";

    }if($doo>3){
      $grid= "small-up-3 medium-up-4";
    }else{
      $grid= "small-up-3 medium-up-4";
    }
   }else{

   if($doo==2){
      $grid= "small-up-3 medium-up-4";
    }elseif($doo>2){
      $grid= "small-up-3 medium-up-5";
    }else{
      $grid= "small-up-3 medium-up-3";
      $sizebox="max-width:870px";
    }

   }

?>

 <div class="row align-middle text-center <?=$grid;?>"  style="<?=$sizebox;?>">
  <a href="<?=site_url('department_personnel/'.$id.'/'.urldecode($n->micro_name));?>" class="columns i-m">
  
  <img src="<?=base_url('img/mi-per.png');?>">
  <h4>ผู้รับผิดชอบงาน</h4>
  </a>



<?php if($doc!=false): ?>
   <?php foreach($doc as $dc): ?>
  <a href="<?=site_url('department_doc/'.$id."/".urldecode($n->micro_name)."/".$dc->id);?>" class="columns i-m">
  <img src="<?=base_url('img/mi-doc.png');?>">
  <h4><?=$dc->name;?></h4>
  </a>
<?php endforeach; ?>
<?php endif;?>

<?php if($num_news>0):?>


<a href="<?=site_url('department_news/'.$id.'/'.urldecode($n->micro_name));?>" class="columns i-m">
  <img src="<?=base_url('img/mi-news.png');?>">
  <h4>ข่าวสาร</h4>
  </a>

<?php endif;?>

  <a href="<?=site_url('department_calendar/'.$id.'/'.urldecode($n->micro_name));?>" class="columns i-m">
  
  <img src="<?=base_url('img/mi-cal.png');?>">
  <h4>ปฎิทินกิจกรรม</h4>
  </a>

</div>
  </div>
</section>

<?php if($doc!=false): ?>
   <?php $ii=1; foreach($doc as $dc):  
    $redoc = $this->db->limit(10)->get_where('doc_micro_file',array('id'=>$dc->id))->result();
    ?>
<section class="row-ta <?=$ii++%2==0?'bg-cf':'bg-ct';?>"> 
<div class="grid-container bg-gray"> 
<div class="box-in">
<h2 class="title-box"><?=$dc->name;?></h2>

<?php if($redoc!=false):?>

<?php foreach($redoc as $rr): ?>
<a target="_blank" href="<?=base_url('file/micro/'.$rr->file);?>" class="grid-x grid-padding-x">
 <div class="cell small-10"> <?=$rr->file_name;?> </div> 
 <div class="cell small-2"> 
 <img src="<?=base_url('img/i-pdf.svg');?>" width="25"> </div> 
 </a> 
<?php endforeach;?>

      <div class="grid-x grid-padding-x ">
          <div class="cell small-12 text-center"> 
           <a href="<?=site_url('department_doc/'.$id."/".urldecode($n->micro_name)."/".$dc->id);?>" class="more-doc">ดูทั้งหมด</a>
          </div>
      </div>
<?php else: ?>
<div class="text-center">
<br/>
--- ยังไม่มีเอกสาร ---
<br/><br/><br/><br/>
</div>

<?php endif;?>


</div>
</div> 
</section>
<?php endforeach;?>
<?php endif;?>

<?php if($ne!=false): ?>
<section class="bg-cf">
  <div class="grid-container bg-w box-in" >
    <div class="grid-x grid-padding-x ">
      <div class="cell small-12 text-center">
      <br/>
      <br/>
        <h3 class="c-cm"><b>ข่าวภายในสำนักงาน</b></h3>
        <hr>
        <br/>
      </div>
    </div>

    <div class="grid-x grid-margin-x grid-padding-x">
      <div class="cell small-12 medium-offset-1 medium-10 ">
        <div class="grid-x grid-margin-x grid-padding-x">
  <?php foreach($ne as $nn):
  $str = strip_tags($nn->detail);
  $string =  iconv_substr($str,0,125, "UTF-8");
    ?>
          <div class="cell small-6 i-inews">
            <div class="pic">
              <img src="<?=base_url('img/micro/news/'.$nn->picture);?>" alt="<?=$nn->title;?>">
            </div>
            <div class="txt">
              <h4><?=$nn->title;?></h4>

              <?=$string;?>...
              <br/><br/>
              <span><img src="<?=base_url('img/view.svg');?>" width="20"> เข้าอ่าน &nbsp;<?=$nn->view;?> </span>
            </div>
            <div class="clearfix foot-n">
              <div class="float-left date-n"><?=date('d/m/Y',strtotime($nn->datetime));?></div>
              <a href="<?=site_url('department_news_detail/'.$id.'/'.urldecode($nn->title)).'/'.$nn->id;?>" class="button float-right">อ่านต่อ</a>
            </div>
          </div>
<?php endforeach; ?>

        </div>
      </div>
    </div>

   <?php if($num_news >= 2): ?>
      <div class="grid-x grid-padding-x ">
          <div class="cell small-12 text-center"> 
           <a href="<?=site_url('department_news/'.$id.'/'.urldecode($n->micro_name));?>" class="more-news">ดูข่าวทั้งหมด</a>
          </div>
      </div>
  <?php endif; ?>

<br><br/>
  </div>
</section>

<?php endif;?>

<?php
if($this->uri->segment(4)=="" ||  $this->uri->segment(5) == ""):
$wantee = date('m').'/'.date('Y');
else:
$wantee = $this->uri->segment(5).'/'.$this->uri->segment(4);
endif;
$this->db->select('*');
$this->db->from('calendar_micro');
$this->db->where("DATE_FORMAT(date,'%m/%Y')", $wantee);
$this->db->where("micro_id", $id);
$ww = $this->db->get()->result();

//echo date('m');
?>
<section class="bg-cf calendar-box" id="calendar">
<div class="grid-container bg-w">
  <div class="grid-x">
      <div class="cell small-12 text-center">
       <h3><b>ปฎิทินกิจกรรม</b></h3>
        <hr/>
      </div>
  </div>
  <div class="grid-x grid-padding-x">
      <div class="cell small-12 medium-8">

<?php 
if($this->uri->segment(4)=="" ||  $this->uri->segment(5) == ""):
  $year = date('Y');
  $month = date('m');
  else:
  $year = $this->uri->segment(4);
  $month = $this->uri->segment(5);
  endif;
$cal_events = array();

if($ww!=false):
foreach ($ww as $e)
{
    if ( date_format(date_create($e->date),"Y") == $year && date_format(date_create($e->date),"m") == $month)
    {       
  $day = date_format( date_create($e->date),"d");
  
  //just made up examples below

  $cal_events[$day] = $e->link; 
    }
}
endif;


echo $this->calendar->generate($this->uri->segment(4), $this->uri->segment(5),$cal_events);?>
      </div>
      <div class="cell small-12 medium-4">
        <div class="box-list-activity">
          <h4><?
$thaiweek=array("วันอาทิตย์","วันจันทร์","วันอังคาร","วันพุธ","วันพฤหัส","วันศุกร์","วันเสาร์");
$thaimonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");


if($this->uri->segment(4)=="" ||  $this->uri->segment(5) == ""):
  echo "สรุปกิจกรรมเดือน ",$thaimonth[date("m")-1] ,date("Y")+543;
  else:
  echo "สรุปกิจกรรมเดือน ",$thaimonth[$this->uri->segment(5)-1] ,$this->uri->segment(4)+543;
  endif;



?> </h4>

<div class="">

  <?php if($ww!=false): ?>
  <ul>
   <?php foreach($ww as $w): ?>

   <li><a href="<?=$w->link;?>"><b><?php
   $date=date_create($w->date);
   echo date_format($date,"d").' '.$thaimonth[date_format($date,"m")-1];
   
   ?></b><br/><?=$w->subject;?></a></li>

   <?php endforeach; ?>

   </ul>

  <?php else: ?>
<br/>
<div class="text-center">
--- ไม่พบกิจกรรมในเดือนนี้ ---
</div>
<br/>
  <?php endif;?>

</div>
        </div>
      </div>
  </div>


</div>
</section>


<?php if($n->poll_status!="close"): ?>
<?php
$su = $this->db->get_where('people_vote',array('micro_id'=>$id))->row();

if($su!=false):
  $vote_sum = $su->people_vote_sum;
else:
  $vote_sum = 0;
endif;
$de = $this->db->get_where('poll',array('name'=>'ดี','micro_id'=>$id))->row();
$demark = $this->db->get_where('poll',array('name'=>'ดีมาก','micro_id'=>$id))->row();
$pochai = $this->db->get_where('poll',array('name'=>'พอใช้','micro_id'=>$id))->row();

if($de!=false): 
$d1 = ($de->vote / $su->people_vote_sum)*100;
else:
$d1 = 0;
endif;

if($demark!=false): 
$d2 = ($demark->vote / $su->people_vote_sum)*100;
else:
$d2 = 0;
endif;

if($pochai!=false): 
$d3 = ($pochai->vote / $su->people_vote_sum)*100;
else:
$d3 = 0;
endif;


?>
<section class="bg-cf poll-vote" id="third">
  <div class="grid-container bg-w box-in" >
  <div class="grid-x">
      <div class="cell small-12 text-center">
       <h3><b>แบบสำรวจ</b></h3>
        <hr/>
      </div>
  </div>

    <div class="grid-x grid-padding-x grid-margin-x">
      <div class="cell small-12 medium-8 medium-offset-2 bg-gray in-vote">


<label>ดีมาก (<?=round($d2);?>%)</label>
<div class="progress" role="progressbar" tabindex="0" aria-valuenow="<?=round($d2);?>" aria-valuemin="0" aria-valuetext="<?=round($d2);?> percent" aria-valuemax="100">
  <div class="progress-meter" style="width: <?=round($d2);?>%"></div>
</div>


<label>ดี (<?=round($d1);?>%)</label>
<div class="progress" role="progressbar" tabindex="0" aria-valuenow="<?=round($d1);?>" aria-valuemin="0" aria-valuetext="<?=round($d1);?> percent" aria-valuemax="100">
  <div class="progress-meter" style="width: <?=round($d1);?>%"></div>
</div>

<label>พอใช้ (<?=round($d3);?>%)</label>
<div class="progress" role="progressbar" tabindex="0" aria-valuenow="<?=round($d3);?>" aria-valuemin="0" aria-valuetext="<?=round($d3);?> percent" aria-valuemax="100">
  <div class="progress-meter" style="width: <?=round($d3);?>%"></div>
</div>

<div class="sum-vote">จำนวนผู้ร่วมโหวตทั้งหมด    <span class="c-ct"><?php  echo $vote_sum; ?></span>   คน

<button class="float-right button large btn-vote" id="vote">โหวต</button>
</div>

      </div>
  </div>

  </div>
</section>
<?php endif;?>


<?=$this->load->view('link_web_micro');?>
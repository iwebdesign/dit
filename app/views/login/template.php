<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>DIT - กรมการค้าภายใน กระทรวงพาณิชย์ </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="manifest" href="<?=base_url('site.webmanifest?v=1');?>">
  <link rel="apple-touch-icon" href="<?=base_url('icon.png?v=1');?>">
  <link rel="stylesheet" href="<?=base_url('css/app.css?v=1');?>">
  <link rel="stylesheet" href="<?=base_url('css/login.css?v=1');?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
</head>

<body>
  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

<div class="row align-center">
  <div class="column small-11 medium-5 i-login shadow">
  <a href="">
        <img src="<?=base_url('img/logo.svg');?>" alt="DIT" width="190">
    </a>

  <?php if(isset($st)): ?>
  <div class="callout alert" data-closable>
  <h5><?=$st;?></h5>
  <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
    <span aria-hidden="true">&times;</span>
  </button>
</div>
  <?php endif; ?>
  <form data-abide novalidate action="<?=site_url('login/checkuser');?>" method="post">
        <label>ชื่อผู้ใช้
          <input type="text" name="username" placeholder="username" aria-describedby="example1Hint1" aria-errormessage="example1Error1" required>
          <span class="form-error" id="example1Error1">
            it's required.
          </span>
        </label>

       <label>รหัสผ่าน
          <input type="password" name="password" id="password" placeholder="password" aria-describedby="example1Hint2" aria-errormessage="example1Error2" required >
          <span class="form-error" id="example1Error2">
          it's required.
          </span>
        </label>

        <button class="button large float-right" type="submit">เข้าสู่ระบบ</button>
 </form>


  </div>
</div>





<script src="<?=base_url('js/app.js?v=1');?>"></script>
<script src="<?=base_url('node_modules/what-input/dist/what-input.js?v=1');?>"></script>
<script src="<?=base_url('node_modules/foundation-sites/dist/js/foundation.js?v=1');?>"></script>
<script>$(document).foundation();</script>
</body>
</html>
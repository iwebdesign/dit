<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<link type="text/css" rel="stylesheet" href="<?=base_url('css/admin.css?v1');?>" />
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<style>
body,html{
	padding:0;
	margin:0;
}
h1{
	font-size:1rem;
}
.logo{
	float:left;
}
.content{
	max-width:1200px;
	width:100%;
	margin:0 auto;
}
.back{
	float:right;
	display:block;
	color:#fff;
	padding:0.5em 1.6em;
	font-size:1rem;
	background-color:#F4A053;
	border-radius:5px;
	margin-top:15px;
	cursor:pointer;
}
.back:hover{
	opacity:0.8;
}
.clear{
	clear:both;
}
</style>

</head>


<body>
	<!-- <div>
		<a href='<?php echo site_url('images_examples/example1')?>'>Example 1 - Simple</a> |
		<a href='<?php echo site_url('images_examples/example2')?>'>Example 2 - Ordering</a> |
		<a href='<?php echo site_url('images_examples/example3/22')?>'>Example 3 - With group id</a> |
		<a href='<?php echo site_url('images_examples/example4')?>'>Example 4 - Images with title</a> | 
		<a href='<?php echo site_url('images_examples/simple_photo_gallery')?>'>Simple Photo Gallery</a>
	</div> -->
	<header>
<div class="container">
  <div class="content">
   <div class="">
   <a href="#" class="logo">
        <img src="<?=base_url('img/logo.svg');?>" alt="DIT" width="100" >
    </a>
	   <div class="back" onclick="goBack()"><< Back </div>
	   <div class="clear"></div>
   </div>
  </div>
</div>
</header>

<?php




$g=$this->db->get_where('news',array('id'=>$this->uri->segment(3)))->row();

?>
	<div style='height:20px;'></div>  

	
	<div class="content">
	<h2>Gallery</h2>
	<h1><?=$g->title;?></h1>
	<hr/>
	<br/>
    <div>
		<?php echo $output; ?>
	</div>
	</div>
<script>
function goBack() {
    window.history.back();
}
</script>
</body>
</html>

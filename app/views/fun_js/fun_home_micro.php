<?php 
if($this->uri->segment(4)!="" ||  $this->uri->segment(5) != ""): ?>
<script>
$(window).scrollTop($('#calendar').offset().top);

</script>


<?php endif;?>

<script>

$(function() {
    
    var $meters = $(".progress > .progress-meter");
    var $section = $('#third');
    var $queue = $({});
    
    function loadDaBars() {
        $meters.each(function() {
            var $el = $(this);
            var origWidth = $el.width();
            $el.width(0);
            $queue.queue(function(next) {
                $el.animate({width: origWidth}, 1000, next);
            });
        });

    }
    
    $(document).bind('scroll', function(ev) {
        var scrollOffset = $(document).scrollTop();
        var containerOffset = $section.offset().top - window.innerHeight;
        if (scrollOffset > containerOffset) {
            loadDaBars();
            // unbind event not to load scrolsl again
            $(document).unbind('scroll');
        }
    });
    
});
</script>
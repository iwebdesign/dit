<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.25.6/dist/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

<script>

$(document)
  // field element is invalid
  .on("invalid.zf.abide", function(ev,elem) {
    console.log("Field id "+ev.target.id+" is invalid");
  })
  // field element is valid
  .on("valid.zf.abide", function(ev,elem) {
    console.log("Field name "+elem.attr('name')+" is valid");
  })
  // form validation failed
  .on("forminvalid.zf.abide", function(ev,frm) {
    console.log("Form id "+ev.target.id+" is invalid");
  })
  // form validation passed, form will submit if submit event not returned false
  .on("formvalid.zf.abide", function(ev,frm) {
    console.log("Form id "+frm.attr('id')+" is valid");
    // ajax post form 
  })
  // to prevent form from submitting upon successful validation
  .on("submit", function(ev) {
    if(ev.target.id!="form2"){
      ev.preventDefault();
      send()
    }
    console.log("Submit for form id "+ev.target.id+" intercepted");
  });


function send(){
let timerInterval
swal({
  title: '<svg xmlns="http://www.w3.org/2000/svg" width="75" height="75" viewBox="0 0 24 24"><path style="fill:#AB7C94;" d="M12 2.02c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 12.55l-5.992-4.57h11.983l-5.991 4.57zm0 1.288l-6-4.629v6.771h12v-6.771l-6 4.629z"/></svg>',
  html: '<i>Sending Mail ...</i>',
  //timer: 2000,
  allowOutsideClick:false,
  backdrop: 'rgba(76,44,17,0.4)',
  onOpen: () => {
    swal.showLoading();


     var values = $('#iform').serialize();

$.ajax({
       url: "<?=site_url('mailer/sendemail');?>",
       type: "post",
       data: values ,
       success: function (response) {
          // you will get response from your php page (what you echo or print)                 
          //swal.close();
  swal({
  title: '<i>Send mail</i>&nbsp; <u> Success</u>',
  type: 'success',
  html:'',
  showCloseButton: true,
  showCancelButton: false,
  focusConfirm: false,
  confirmButtonText:'<i class="fa fa-thumbs-up"></i> OK',
  onClose: () => {
    $('#iform').foundation('resetForm');
  }
});
       },
       error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
       }


   });

  },
  onClose: () => {

  }
}).then((result) => {
  if (
    // Read more about handling dismissals
    result.dismiss === swal.DismissReason.timer
  ) {
    console.log('I was closed by the timer')
  }
})


}

function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}
document.onkeypress = stopRKey;

</script>
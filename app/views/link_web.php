<?php
$gg=$this->db->order_by('order_number','asc')->get('link_relate')->result();
$ll=$this->db->order_by('order_number','asc')->get('link_web')->result();
?>
<section>
<div class="grid-container box-in" >
    <div class="grid-x">
      <div class="cell small-12">
        <div class="sec-bottom">
          <h3>เว็บไซต์์ที่เกี่ยวข้อง</h3>
            <hr>

<?php if($gg!=false): ?>
<div class="text-center link-img">
<?php foreach($gg as $g): ?>
<a href="<?=$g->link;?>" target="_blank">
<img src="<?=base_url('img/'.$g->picture);?>">&nbsp;
</a>
<?php endforeach;?>
</div>
<?php endif;?>
<br/>
<br/>

<?php if($ll!=false): ?>
<h3>เชื่อมโยงไปยังเว็บไซต์</h3>
<hr>

<div class="text-center link-img link-website">

<?php foreach($ll as $l): ?>
<a href="<?=$l->link;?>" target="_blank"><img src="<?=base_url('img/'.$l->picture);?>"></a>&nbsp;
<?php endforeach;?>

</div>

<?php endif;?>
        </div>
      </div>
    </div>
  </div>
</section>
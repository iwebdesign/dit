<?php
$st = $this->db->get('setting')->row();
?>
<!doctype html>
<html class="no-js" lang="th">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?=$st->title_web;?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="<?=site_url('logo.ico?v=1');?>">
  <link rel="manifest" href="<?=base_url('site.webmanifest?v=1');?>">
  <link rel="apple-touch-icon" href="<?=base_url('icon.png?v=1');?>">
  <link rel="stylesheet" href="<?=base_url('css/micro.css?v=99');?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link rel="stylesheet" href="<?=base_url('css/lightslider.css?v=1');?>">

<?php if($st->google_analytic!=""): ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124263676-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124263676-1');
</script>
<?php endif;?>
</head>
<body>
<?php
//$this->load->model('visit_model');
$this->visit_micro_model->VisitorCounter($this->uri->segment(2));
?>
  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
<div class="overlay"></div>
<?=$this->load->view('layout/header_micro');?>
<?=$this->load->view('micro/'.$content);?>


<div class="copyright text-center">
© 2018 กองส่งเสริมการค้าสินค้าเกษตร 1 กรมการค้าภายใน กระทรวงพาณิชย์
</div>



<script src="<?=base_url('js/app.js?v=1');?>"></script>
<script src="<?=base_url('node_modules/what-input/dist/what-input.js?v=1');?>"></script>
<script src="<?=base_url('node_modules/foundation-sites/dist/js/foundation.js?v=1');?>"></script>
<script type="text/javascript" src="<?=base_url('js/jquery.lazy.min.js?v=1');?>"></script>
<script src="<?=base_url('js/wow.js');?>"></script>
<script src="<?=base_url('js/lightslider.js');?>"></script>
<script>$(document).foundation();
new WOW().init();
   $(function(){
      if (!Modernizr.svg) {
        $('img[src*=".svg"]').attr('src', function() {
          return $(this).attr('src').replace('.svg', '.png');
        });
      }else{
          console.log('ok');
      }
       $('.lazy').lazy();
    });
</script>


      <script type="text/javascript">
    $(document).ready(function() {
      var slider = $("#light-slider").lightSlider({
            item: 3,
            autoWidth: false,
            slideMove: 1,
            slideMargin: 20,
            pager: true,
            controls:false,
            adaptiveHeight:true,
            onSliderLoad: function() {
            $('#light-slider').removeClass('cs-hidden');
           },
           responsive : [
            {
                breakpoint:1100,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:15,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:929,
                settings: {
                    item:3,
                    slideMove:4,
                    slideMargin:15,
                    pager:true,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:780,
                settings: {
                    item:3,
                    slideMove:3,
                    pager:true,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:690,
                settings: {
                    item:2,
                    slideMove:2,
                    pager:true,
                    adaptiveHeight:true,
                  }
            }
        ]
        });
  
  if(slider.getTotalSlideCount()<6){
    $('#goToPrevSlide').hide();
    $('#goToNextSlide').hide();
  }

   $('#goToPrevSlide').click(function(){
        slider.goToPrevSlide(); 
    });
    $('#goToNextSlide').click(function(){
        slider.goToNextSlide(); 
    });
    });
$(document).ready(function() {$('#imageGallery').lightSlider({speed:2200,auto:true, adaptiveHeight:true,gallery:true,item:1,loop:true,thumbItem:6,slideMargin:0,enableDrag:true,pauseOnHover:true  });  });
function myFunction(x) {x.classList.toggle("change");}
$('.container1').click(function(){$('.menu-main').toggle();$('.overlay').toggle();$('body,html').toggleClass('noscroll');});
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script>

  $('#vote').on('click',function(){
    swal({
  title: 'เลือกคะแนนโหวด',
  confirmButtonText:  'ตกลง',
  input: 'radio',
  showLoaderOnConfirm: true,
  inputOptions: {
      'ดีมาก': 'ดีมาก',
      'ดี': 'ดี',
      'พอใช้': 'พอใช้'
    },
    allowOutsideClick:false,
    preConfirm: (vote) => {
    return fetch('<?=site_url('poll');?>/index/'+vote+'/<?=$this->uri->segment(2);?>')
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        return response.json()
      })
      .catch(error => {
        swal.showValidationError(
          `Request failed: ${error}`
        )
      })
  },
  inputValidator: (value) => {
    return !value && 'กรุณาเลือกผลโหวต!'
  }
}).then((result) => {
  if (result.value) {
  swal({
  type: 'success',
  title: 'คุณโหวต '+result.value.sta+'<br>ขอบคุณสำหรับผลโหวตของท่าน',
  showConfirmButton: true,
  allowOutsideClick:false,
    }).then((res) => {
      location.reload();
     });
  } 
});
});

$('.back').click(function() {
    window.history.back();
});
</script>
<?php if(isset($funjs)):  echo $this->load->view('fun_js/'.$funjs); endif; ?>
</body>
</html>

<?php
$idpro = $this->uri->segment(2);
$sq = $this->db->get_where('counter_micro',array('DATE'=>date("Y-m-d"),'IP'=>$_SERVER["REMOTE_ADDR"],'micro_id'=>$idpro))->num_rows();
if($sq<1):
  $strSQL = " INSERT INTO counter_micro (DATE,IP,micro_id) VALUES ('".date("Y-m-d")."','".$_SERVER["REMOTE_ADDR"]."','".$idpro."') ";
  $this->db->query($strSQL);
endif;


//*** Select วันที่ในตาราง Counter ว่าปัจจุบันเก็บของวันที่เท่าไหร่  ***//
  //*** ถ้าเป็นของเมื่อวานให้ทำการ Update Counter ไปยังตาราง daily และลบข้อมูล เพื่อเก็บของวันปัจจุบัน ***//
  

	// $strSQL = "SELECT * FROM counter_micro where micro_id = '".$idpro."' LIMIT 0,1";
	// $objQuery = $this->db->query($strSQL);
	$obj =  $this->db->get_where('counter_micro',array('micro_id'=>$idpro,'DATE !='=>date("Y-m-d")))->row();
	if($obj!=false)
	{
		//*** บันทึกข้อมูลของเมื่อวานไปยังตาราง daily ***//
		$strSQL = " INSERT INTO daily_micro (DATE,NUM,micro_id) SELECT '".date('Y-m-d',strtotime("-1 day"))."',COUNT(*) AS intYesterday,'".$idpro."' FROM  counter_micro WHERE 1 AND DATE = '".date('Y-m-d',strtotime("-1 day"))."' AND micro_id = '".$idpro."' ";
		$this->db->query($strSQL);

		//*** ลบข้อมูลของเมื่อวานในตาราง counter ***//
    $strSQL = " DELETE FROM counter_micro WHERE DATE != '".date("Y-m-d")."' ";
    $this->db->query($strSQL);
  }

  // $strSQL = " INSERT INTO counter_micro (DATE,IP,micro_id) VALUES ('".date("Y-m-d")."','".$_SERVER["REMOTE_ADDR"]."','".$idpro."') ";
  // $this->db->query($strSQL);



?>
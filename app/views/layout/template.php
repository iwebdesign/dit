<?php
$st = $this->db->get('setting')->row();
$so = $this->db->get('social')->row();
?>
<!doctype html>
<html class="no-js" lang="th">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?=$st->title_web;?> </title>
  <meta name="description" content="">
<?php  if($this->uri->segment(1)=='agri_price'): ?>
<meta name="viewport" content="width=1280">
  
<?php else: ?>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php endif;?>
  <link rel="shortcut icon" href="<?=site_url('logo.ico?v=1');?>">
  <link rel="manifest" href="<?=base_url('site.webmanifest?v=1');?>">
  <link rel="apple-touch-icon" href="<?=base_url('icon.png?v=1');?>">
  <link rel="stylesheet" href="<?=base_url('css/app.css?v=1');?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link rel="stylesheet" href="<?=base_url('css/lightslider.css?v=1');?>">

<?php if($st->google_analytic!=""): ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124263676-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124263676-1');
</script>
<?php endif;?>
</head>
<body>
<?php
//$this->load->model('visit_model');
$this->visit_model->VisitorCounter();
?>
  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->
<div class="overlay"></div>
<?=$this->load->view('layout/header');?>
<?=$this->load->view('frontend/'.$content);?>
<footer class="bg-w">
<ul class="menu-footer">
<li class="<?=$this->uri->segment(1)=="home"||$this->uri->segment(1)==false?'active':'';?>">
    <a href="<?=site_url();?>">หน้าหลัก</a>
  </li>
<li class="<?=$this->uri->segment(1)=="about_us"?'active':'';?>">
    <a href="<?=site_url('about_us');?>">เกี่ยวกับเรา</a>
  </li>
  <li class="<?=$this->uri->segment(1)=="personnel"?'active':'';?>">
    <a href="<?=site_url('personnel');?>">บุคลากร</a>
  </li>
  <li class="<?=$this->uri->segment(1)=="contact_us"?'active':'';?>">
    <a href="<?=site_url('contact_us');?>">ติดต่อเรา</a>
  </li>  
  <li class="<?=$this->uri->segment(1)=="complaint"?'active':'';?>">
    <a href="<?=site_url('complaint');?>">แจ้งปัญหา</a>
  </li>
</ul>


<div class="social ">
   <a href="<?=$so->line;?>" class="zoom wow slideInLeft" ><span class="icon-line"></span></a>
   <a href="<?=$so->facebook;?>" class="zoom wow slideInDown" ><span class="icon-facebook"></span></a>
   <a href="mailto:<?=$so->mail;?>" class="zoom wow slideInRight" ><span class="icon-mail"></span></a>
</div>

</footer>

<div class="copyright text-center">
© 2018 กองส่งเสริมการค้าสินค้าเกษตร 1 กรมการค้าภายใน กระทรวงพาณิชย์
</div>



<script src="<?=base_url('js/app.js?v=1');?>"></script>
<script src="<?=base_url('node_modules/what-input/dist/what-input.js?v=1');?>"></script>
<script src="<?=base_url('node_modules/foundation-sites/dist/js/foundation.js?v=1');?>"></script>
<script type="text/javascript" src="<?=base_url('js/jquery.lazy.min.js?v=1');?>"></script>
<script src="<?=base_url('js/wow.js');?>"></script>
<script src="<?=base_url('js/lightslider.js');?>"></script>
<script>$(document).foundation();
new WOW().init();
   $(function(){
      if (!Modernizr.svg) {
        $('img[src*=".svg"]').attr('src', function() {
          return $(this).attr('src').replace('.svg', '.png');
        });
      }else{
          console.log('ok');
      }
       $('.lazy').lazy();
    });
</script>


      <script type="text/javascript">
    $(document).ready(function() {
      var slider = $("#light-slider").lightSlider({
            item: 4,
            autoWidth: false,
            slideMove: 1,
            slideMargin: 20,
            pager: false,
            controls:false,
            adaptiveHeight:true,
            onSliderLoad: function() {
            $('#light-slider').removeClass('cs-hidden');
           },
           responsive : [
            {
                breakpoint:1100,
                settings: {
                    item:4,
                    slideMove:1,
                    slideMargin:15,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:929,
                settings: {
                    item:4,
                    slideMove:4,
                    slideMargin:15,
                    pager:true,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:780,
                settings: {
                    item:3,
                    slideMove:3,
                    pager:true,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:690,
                settings: {
                    item:2,
                    slideMove:2,
                    pager:true,
                    adaptiveHeight:true,
                  }
            }
        ]
        });
  
  if(slider.getTotalSlideCount()<6){
    $('#goToPrevSlide').hide();
    $('#goToNextSlide').hide();
  }

   $('#goToPrevSlide').click(function(){
        slider.goToPrevSlide(); 
    });
    $('#goToNextSlide').click(function(){
        slider.goToNextSlide(); 
    });
    });
$(document).ready(function() {$('#imageGallery').lightSlider({speed:2200,auto:true, adaptiveHeight:true,gallery:true,item:1,loop:true,thumbItem:6,slideMargin:0,enableDrag:true,pauseOnHover:true  });  });
function myFunction(x) {x.classList.toggle("change");}
$('.container1').click(function(){$('.menu-main').toggle();$('.overlay').toggle();$('body,html').toggleClass('noscroll');});
$('.back').click(function() {
    window.history.back();
});
</script>
<?php if(isset($funjs)):  echo $this->load->view('fun_js/'.$funjs); endif; ?>
</body>
</html>
<?php


//*** Select วันที่ในตาราง Counter ว่าปัจจุบันเก็บของวันที่เท่าไหร่  ***//
	//*** ถ้าเป็นของเมื่อวานให้ทำการ Update Counter ไปยังตาราง daily และลบข้อมูล เพื่อเก็บของวันปัจจุบัน ***//
	$strSQL = " SELECT DATE FROM counter LIMIT 0,1";
	$objQuery = $this->db->query($strSQL);
	$obj =  $objQuery->row();
	if($obj->DATE != date("Y-m-d"))
	{
		//*** บันทึกข้อมูลของเมื่อวานไปยังตาราง daily ***//
		$strSQL = " INSERT INTO daily (DATE,NUM) SELECT '".date('Y-m-d',strtotime("-1 day"))."',COUNT(*) AS intYesterday FROM  counter WHERE 1 AND DATE = '".date('Y-m-d',strtotime("-1 day"))."'";
		$this->db->query($strSQL);

		//*** ลบข้อมูลของเมื่อวานในตาราง counter ***//
		$strSQL = " DELETE FROM counter WHERE DATE != '".date("Y-m-d")."' ";
		$this->db->query($strSQL);
  }
  
$sq = $this->db->get_where('counter',array('DATE'=>date("Y-m-d"),'IP'=>$_SERVER["REMOTE_ADDR"]))->row();

if($sq==false):
$strSQL = " INSERT INTO counter (DATE,IP) VALUES ('".date("Y-m-d")."','".$_SERVER["REMOTE_ADDR"]."') ";
$this->db->query($strSQL);
endif;
?>
<header>
<div class="grid-container">
  <div class="grid-x">
    <div class="cell small-4 medium-3 ">
    <a href="<?=site_url('home');?>" class="logo">
        <img src="<?=base_url('img/logo.svg');?>" alt="DIT" >
    </a>
    </div>
    <div class="cell small-8 medium-8 medium-offset-1">
<div class="container1 show-for-small-only" onclick="myFunction(this)">
  <div class="bar1"></div>
  <div class="bar2"></div>
  <div class="bar3"></div>
</div>

<div class="searchweb hide-for-small-only">        
<form action="<?=site_url('search');?>" method="get" id="form2">
<input type="text" name="q" placeholder="ค้นหา">
<input type="hidden" name="hl" value="th">
<button type="submit"></button>
</form>
</div>

<div class="clear"></div>

<ul id="myDIV" class=" vertical medium-horizontal menu menu-main" data-responsive-menu="drilldown medium-dropdown" data-hide-for="medium">
 <li class="<?=$this->uri->segment(1)=="home"||$this->uri->segment(1)==false?'active':'';?>">
    <a href="<?=site_url('home');?>">หน้าหลัก</a>
  </li>
<li class="<?=$this->uri->segment(1)=="about_us"?'active':'';?>">
    <a href="<?=site_url('about_us');?>">เกี่ยวกับเรา</a>
  </li>
  <li class="<?=$this->uri->segment(1)=="personnel"?'active':'';?>">
    <a href="<?=site_url('personnel');?>">บุคลากร</a>
  </li>
  <li class="<?=$this->uri->segment(1)=="contact_us"?'active':'';?>">
    <a href="<?=site_url('contact_us');?>">ติดต่อเรา</a>
  </li>  
  <li class="<?=$this->uri->segment(1)=="complaint"?'active':'';?>">
    <a href="<?=site_url('complaint');?>">แจ้งปัญหา</a>
  </li>
</ul>

</div>
  </div>
</div>
</header>
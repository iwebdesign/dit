<?php
$st = $this->db->get('setting')->row();
?>
<!doctype html>
<html class="no-js" lang="th">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?=$st->title_web;?> </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="<?=site_url('logo.ico?v=1');?>">
  <link rel="manifest" href="<?=base_url('site.webmanifest?v=1');?>">
  <link rel="apple-touch-icon" href="<?=base_url('icon.png?v=1');?>">
  <link rel="stylesheet" href="<?=base_url('css/app.css?v=1');?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link rel="stylesheet" href="<?=base_url('css/lightslider.css?v=1');?>">

<?php if($st->google_analytic!=""): ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124263676-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124263676-1');
</script>
<?php endif;?>
</head>
<body>
<?php 
$this->db->where('status !=','close');
$g= $this->db->get('intro')->row();
?>
<section style="max-width:1000px;">
<div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
  <div class="orbit-wrapper">
    <ul class="orbit-container">
      <li class="orbit-slide">
      <a href="<?=site_url('home');?>">
          <img class="orbit-image" src="<?=base_url('img/'.$g->picture);?>" alt="Space">
      </a>
      </li>
    </ul>
  </div>
</div>
</section>
<div class="row align-center">
  <div class="column small-6 medium-3"><br/><a href="<?=site_url('home');?>" class="button expanded bg-ct" >เข้าสูเว็บไซต์</a></div>
</div>
<script src="<?=base_url('js/app.js?v=1');?>"></script>
<script src="<?=base_url('node_modules/what-input/dist/what-input.js?v=1');?>"></script>
<script src="<?=base_url('node_modules/foundation-sites/dist/js/foundation.js?v=1');?>"></script>
<script type="text/javascript" src="<?=base_url('js/jquery.lazy.min.js?v=1');?>"></script>
<script src="<?=base_url('js/wow.js');?>"></script>
<script src="<?=base_url('js/lightslider.js');?>"></script>
<script>$(document).foundation();
new WOW().init();
   $(function(){
      if (!Modernizr.svg) {
        $('img[src*=".svg"]').attr('src', function() {
          return $(this).attr('src').replace('.svg', '.png');
        });
      }else{
          console.log('ok');
      }
       $('.lazy').lazy();
    });
</script>


      <script type="text/javascript">
    $(document).ready(function() {
      var slider = $("#light-slider").lightSlider({
            item: 4,
            autoWidth: false,
            slideMove: 1,
            slideMargin: 20,
            pager: false,
            controls:false,
            adaptiveHeight:true,
            onSliderLoad: function() {
            $('#light-slider').removeClass('cs-hidden');
           },
           responsive : [
            {
                breakpoint:1100,
                settings: {
                    item:4,
                    slideMove:1,
                    slideMargin:15,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:929,
                settings: {
                    item:4,
                    slideMove:4,
                    slideMargin:15,
                    pager:true,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:780,
                settings: {
                    item:3,
                    slideMove:3,
                    pager:true,
                    adaptiveHeight:true,
                  }
            },
            {
                breakpoint:690,
                settings: {
                    item:2,
                    slideMove:2,
                    pager:true,
                    adaptiveHeight:true,
                  }
            }
        ]
        });
  
  if(slider.getTotalSlideCount()<6){
    $('#goToPrevSlide').hide();
    $('#goToNextSlide').hide();
  }

   $('#goToPrevSlide').click(function(){
        slider.goToPrevSlide(); 
    });
    $('#goToNextSlide').click(function(){
        slider.goToNextSlide(); 
    });
    });
$(document).ready(function() {$('#imageGallery').lightSlider({speed:2200,auto:true, adaptiveHeight:true,gallery:true,item:1,loop:true,thumbItem:6,slideMargin:0,enableDrag:true,pauseOnHover:true  });  });
function myFunction(x) {x.classList.toggle("change");}
$('.container1').click(function(){$('.menu-main').toggle();$('.overlay').toggle();$('body,html').toggleClass('noscroll');});
$('.back').click(function() {
    window.history.back();
});
</script>
<?php if(isset($funjs)):  echo $this->load->view('fun_js/'.$funjs); endif; ?>
</body>
</html>

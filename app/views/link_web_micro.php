<?php
// microsite
//$n = $this->db->get_where('microsite',array('micro_id'=>$id))->row();
$ll=$this->db->order_by('order_number','asc')->get('link_web')->result();

$sl = $this->db->order_by('id','desc')->get_where('link_micro',array('micro_id'=>$id,'status !='=>'close'))->result();
?>

<section class="box-link-micro bg-ct">
<div class="grid-container box-in bg-gray sec-bottom" >
    <div class="grid-x">
<?php if($sl!=false): ?>
      <div class="cell small-12 medium-7">
  <h3>เว็บไซต์์ที่เกี่ยวข้อง</h3>
<hr>
<br/>

<ul id="light-slider" class="cs-hidden">
<?php foreach($sl as $s): ?>
    <li>
       <a target="_blank" href="<?=$s->link;?>">
       <img src="<?=base_url('img/micro/news/'.$s->picture);?>">
       </a>
    </li>
<?php endforeach;?>
</ul>
</div>
<?php endif;?>

<?php     
$idpro = $this->uri->segment(2);  
     $this->db->select_sum('NUM');          
     $re = $this->db->get_where('daily_micro',array('micro_id'=>$idpro))->row();  
     
     	// This Month //
       $strSQL = " SELECT SUM(NUM) AS CountMonth ,micro_id FROM daily_micro WHERE DATE_FORMAT(DATE,'%Y-%m')  = '".date('Y-m')."' AND micro_id = '".$idpro."' ";
       $objQuery = $this->db->query($strSQL);
       $objResult = $objQuery->row();
       $strThisMonth = $objResult->CountMonth;
    
?>
<div class="cell small-12 <?=$sl!=false?'medium-5':'medium-5';?>">
<h3>สถิติผู้เข้าชม</h3>
<hr>
<br/>

<div class="grid-container text-center">
  <div class="grid-x grid-padding-x stat">
  <div class="cell small-12 medium-6 bg-w i-stat1">
  <?=str_pad($this->visit_micro_model->getAmountVisitors($this->uri->segment(2)),6,"0",STR_PAD_LEFT);?>
  </div>
<div class="cell small-12 medium-6 bg-ct i-stat2">
จำนวนผู้ชมขณะนี้
</div>

  </div>
</div>

<br>

<div class="grid-container text-center">
  <div class="grid-x grid-padding-x stat">
  <div class="cell small-12 medium-6 bg-w i-stat1">
  <?=str_pad($strThisMonth,6,"0",STR_PAD_LEFT);?>
  </div>
<div class="cell small-12 medium-6 bg-cm i-stat2">
จำนวนผู้ชมเดือนนี้
</div>

  </div>
</div>

<br>

<div class="grid-container text-center">
  <div class="grid-x grid-padding-x stat">
  <div class="cell small-12 medium-6 bg-w i-stat1">
  <?=str_pad($re->NUM,6,"0",STR_PAD_LEFT);?>
  </div>
<div class="cell small-12 medium-6 bg-cs i-stat2">
จำนวนผู้ชมทั้งหมด
</div>

  </div>
</div>
<br/>
<br/><br/>

</div>


<div class="cell small-12 <?=$sl!=false?'':'medium-7';?>">
<?php if($ll!=false): ?>
  <h3>เชื่อมโยงไปยังเว็บไซต์</h3>
<hr>


<div class="text-center link-img link-website">
<?php foreach($ll as $l): ?>
<a href="<?=$l->link;?>"><img src="<?=base_url('img/'.$l->picture);?>"></a>&nbsp;
<?php endforeach;?>
</div>
<?php endif;?>
</div>
      </div>
    </div>
  </div>
</section>
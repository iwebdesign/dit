<section class="h-page lazy" data-src="<?=base_url('img/bg-h.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12 logo-com">
        <img src="<?=base_url('img/complaint-logo.png');?>" class="float-right" style="max-width:620px;">
      </div>
    </div>
  </div>
</section>

<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1>รับแจ้งปัญหาการใช้งานระบบ</h1>
         <hr>
      </div>
    </div>
  </div>
</section>


<section class="box-complaint">
<div class="grid-container b-in lazy" data-src="<?=base_url('img/bg-complaint.jpg');?>">
<form data-abide novalidate id="iform">
    <div class="grid-x grid-padding-x margin-top-2">
    <div class="cell small-2 medium-4 text-right">
        วันที่ :
    </div>
    <div class="cell small-10 medium-4"><input type="date" placeholder="" name="date" aria-describedby="example1Hint7" aria-errormessage="example1Error7" required>
    <span class="form-error" id="example1Error6">
        กรุณากรอกข้อมูล
   </span>
    </div>

    </div>

    <div class="grid-x grid-padding-x margin-top-2">
    <div class="cell small-2 medium-4 text-right">
       ชื่อเรื่อง :
    </div>
    <div class="cell small-10 medium-7"><input type="text" placeholder="" name="subject" aria-describedby="example1Hint6" aria-errormessage="example1Error6" required>
    
    <span class="form-error" id="example1Error6">
        กรุณากรอกข้อมูล
   </span>
    </div>

    </div>

    <div class="grid-x grid-padding-x margin-top-2">
    <div class="cell small-2 medium-4 text-right">
       รายละเอียด :
    </div>
    <div class="cell small-10 medium-7"><textarea rows="5" name="message" aria-describedby="example1Hint5" aria-errormessage="example1Error5" required></textarea>
    <span class="form-error" id="example1Error5">
        กรุณากรอกข้อมูล
   </span>
    </div>

    </div>

   <div class="grid-x grid-padding-x margin-top-2">
    <div class="cell small-2 medium-4 text-right">
       ผู้แจ้ง :
    </div>
    <div class="cell small-10 medium-7"><input type="text" placeholder="" name="name" aria-describedby="example1Hint4" aria-errormessage="example1Error4" required>
    <span class="form-error" id="example1Error4">
        กรุณากรอกข้อมูล
   </span>
    
    
    </div>

    </div>


   <div class="grid-x grid-padding-x margin-top-2">
    <div class="cell small-2 medium-4 text-right">
       ที่อยู่ :
    </div>
    <div class="cell small-10 medium-7"><textarea rows="5" name="address" aria-describedby="example1Hint3" aria-errormessage="example1Error3" required></textarea>
    <span class="form-error" id="example1Error3">
        กรุณากรอกข้อมูล
   </span>
    
    
    
    </div>

    </div>

    <div class="grid-x grid-padding-x margin-top-2">
    <div class="cell small-2 medium-4 text-right">
       อีเมล :
    </div>
    <div class="cell small-10 medium-4"><input type="email" name="email" placeholder="" pattern="email" aria-describedby="example1Hint1" aria-errormessage="example1Error1" required>
    <span class="form-error" id="example1Error1">
        กรุณากรอกข้อมูล หรือ กรอกอีเมลให้ถูกต้อง
   </span>
    
    </div>

    </div>

    <div class="grid-x grid-padding-x margin-top-2">
    <div class="cell small-2 medium-4 text-right">
      โทรศัพท์ :
    </div>
    <div class="cell small-10 medium-4"><input type="text" name="tel" placeholder=""   aria-describedby="example1Hint2" aria-errormessage="example1Error2"  required>
    <span class="form-error" id="example1Error2">
        กรุณากรอกข้อมูล
   </span>
    
    
    
    </div>

    </div>

   <div class="grid-x grid-padding-x margin-top-2">
    <div class="cell small-2 medium-4">
      
    </div>
    <div class="cell small-10 medium-7">
        <button class="button warning large float-right" type="submit">ส่งคำร้อง</button>
    </div>

    </div>
</form>
  </div>
</section>

<?=$this->load->view('link_web');?>


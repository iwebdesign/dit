<?php
$g = $this->db->get('about')->row();
?>
<section class="h-page lazy" data-src="<?=base_url('img/bg-h.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
        <img src="<?=base_url('img/depart-logo.png');?>" class="float-right">
      </div>
    </div>
  </div>
</section>

<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1><?=$g->title_1;?></h1>
         <h2><?=$g->title_2;?></h2>
         <hr>
      </div>
    </div>
  </div>
</section>

<section class="detail-about">
<div class="grid-container" >
    <div class="grid-x grid-padding-x">
      <div class="cell small-12">
      <?=$g->detail;?>
      </div>
    </div>
  </div>
</section>


<?=$this->load->view('link_web');?>
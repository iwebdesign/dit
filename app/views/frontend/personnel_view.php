<?php 
$gg  = $this->db->get_where('personnel',array('level'=>'boss'))->result();
$gg1 = $this->db->get_where('personnel',array('level'=>'staff'))->result();
?>
<section class="h-page lazy" data-src="<?=base_url('img/bg-h.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
        <img src="<?=base_url('img/per-logo.png');?>" class="float-right">
      </div>
    </div>
  </div>
</section>
<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1>บุคลากร</h1>
         <hr>
      </div>
    </div>
  </div>
</section>

<section class="page-per">
<div class="grid-container" >
<div class="grid-x grid-margin-x">
<div class="cell small-12 medium-10 medium-offset-1">

<?php if($gg!=false): ?>

<?php foreach($gg as $r): ?>
<div class="row align-center">
  <div class="column small-6 medium-4 i-per">
  
  <?php if($r->picture!=""): ?>
  <img src="<?=base_url('img/personnel/'.$r->picture);?>" >
  <?php else: ?>
  <img src="<?=base_url('img/per1.jpg');?>" >
  <?php endif;?>

    <div class="b-name">
       <p><?=$r->name;?></p>
       <p class="c-cs">ตำแหน่ง  <?=$r->position;?></p>
       <p class="c-cs">หนัาที่  <?=$r->mission;?></p>
    </div>
</div>
</div>
<?php endforeach; ?>
<?php endif;?>



<?php if($gg1!=false): ?>
<div class="row align-spaced">


<?php foreach($gg1 as $r1): ?>
<div class="column small-6 medium-4 i-per">
<?php if($r1->picture!=""): ?>
  <img src="<?=base_url('img/personnel/'.$r1->picture);?>" >
  <?php else: ?>
  <img src="<?=base_url('img/per1.jpg');?>" >
  <?php endif;?>
  <div class="b-name">
       <p><?=$r1->name;?></p>
       <p class="c-cs">ตำแหน่ง  <?=$r1->position;?></p>
       <p class="c-cs">หนัาที่  <?=$r1->mission;?></p>
    </div>
    </div>
 <?php endforeach; ?>

</div>

<?php endif;?>
</div>
</div>
</div>
</section>

<?=$this->load->view('link_web');?>
<section class="h-page lazy" data-src="<?=base_url('img/bg-h1.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12 logo-com">
        <img src="<?=base_url('img/agri-logo.png');?>" class="float-right" style="max-width:500px;">
      </div>
    </div>
  </div>
</section>
<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1>ราคาสินค้าเกษตร</h1>
      </div>
    </div>
 <div class="row align-justify">
  <div class="column small-12 medium-6" style="padding:0;">ทั้งหมด : <?=$count;?></div>
  <div class="column small-12 medium-3" style="padding:0;"><?=$this->pagination->create_links();?></div>
</div>
<div class="grid-x">
<div class="cell small-12">
         <hr/>
</div>
</div>


  </div>
</section>

<section class="row-ta">
<div class="grid-container">
<div class="grid-x grid-margin-x grid-padding-x"  data-equalizer>
<?php if($pp!=false): ?>
<?php foreach($pp as $p): ?>
      <a href="<?=site_url('price_agri/page/'.$p->id.'/'.urlencode(str_replace('/','-',$p->name)));?>" class="cell small-6 medium-4 i-p">
        <div class="pic">
          <img data-src="<?=base_url('img/price/'.$p->picture);?>" alt="<?=$p->name;?>" class="lazy">
        </div>
        <div class="title-p">
          <?=$p->name;?>
        </div>
      </a>
<?php endforeach;?>
<?php endif;?>

    </div>
</div>
</section>

<?=$this->load->view('link_web');?>
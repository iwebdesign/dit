<?php
$ge = $this->db->order_by('order_number','asc')->get_where('slide_home',array('status !='=>'close'));
$nSlide= $ge->num_rows();
$g  = $ge->result();
$m = $this->db->get_where('microsite',array('micro_status !='=>'close'))->result();
?>
<section class="slide-h">
  <div class="orbit" role="region" data-auto-play="true" aria-label="Favorite Space Pictures" data-orbit>
    <div class="orbit-wrapper">
    <?php if($nSlide > 1): ?>
      <div class="orbit-controls">
        <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
        <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
      </div>
    <?php endif; ?>
      <?php if($g!=false): ?>
      <ul class="orbit-container">
        <?php $b=0; foreach($g as $r): ?>
        <li class="<?=$b++==0?'is-active':'';?> orbit-slide">
          <a href="<?=$r->link;?>" class="orbit-figure">
          <img class="orbit-image" src="<?=base_url('img/slide-home/'.$r->picture);?>" alt="Space">
          </a>
        </li>
        <?php endforeach; ?>
      </ul>

    </div>
    <nav class="orbit-bullets">
    <?php $i=0; foreach($g as $r):  ?>
      <button class="<?=$i==0?'is-active':'';?>" data-slide="<?=$i++;?>"></button>
      <?php endforeach; ?>

    </nav>
    <?php endif; ?>
  </div>
</section>
<section class="all-m">
  <div class="grid-container">
    <div class="grid-x grid-padding-x small-up-2 medium-up-5 large-up-5" data-equalizer>

<?php if($m!=false): ?>

   <?php foreach($m as $mm): ?>
      <a href="<?=site_url('department/'.$mm->micro_id.'/'.urldecode($mm->micro_name));?>" class="cell i-group">
        <div class="icon-web ">
          <span class="<?=$mm->micro_picture;?>"></span>
          <div class="text-in">
            <?=$mm->micro_name;?>
          </div>
        </div>
      </a>
<?php endforeach; ?>
<?php endif;?>

    </div>
  </div>
</section>

<?php
$pp = $this->db->order_by('id','desc')->get_where('price_agri',array('status !='=>'close'))->result();
?>

<section class="price-agri bg-cs price-main-box">
  <div class="grid-container bg-w">
    <div class="grid-x">
      <div class="cell small-12 text-center">
        <h3><b>ราคาสินค้าเกษตร</b></h3>
        <hr/>
      </div>
    </div>
    <div class="grid-x grid-margin-x grid-padding-x" data-equalizer>
<!--
<?php if($pp!=false): ?>
<?php foreach($pp as $p): ?>
<a href="<?=site_url('price_agri/page/'.$p->id.'/'.urlencode(str_replace('/','-',$p->name)));?>" class="cell small-6 medium-4 i-p">
        <div class="pic">
          <img data-src="<?=base_url('img/price/'.$p->picture);?>" alt="<?=$p->name;?>" class="lazy">
        </div>
        <div class="title-p">
          <?=$p->name;?>
        </div>
      </a>
<?php endforeach;?>
<?php endif;?>
-->

<a href="<?=site_url('agri_price');?>" class="cell small-12 medium-8 medium-offset-2 i-p">
        <div class="pic">
          <img data-src="<?=base_url('img/agri-i.jpg');?>" alt="ราคาเกษตร" class="lazy">
        </div>
        <div class="title-p">
        รายงานการติดตามภาวะตลาดสินค้าในกรุงเทพมหานคร
        </div>
</a>

    </div>
<!--
      <div class="grid-x grid-padding-x grid-margin-x">
          <div class="cell small-12 text-center"> 
<br/>
     <a href="<?=site_url('price_agri');?>" class="more-news">อ่านทั้งหมด</a>
     <br/>
          </div>
      </div>

      -->
      
  </div>
</section>

<?php 

$a=$this->db->get('about')->row();

?>
<section class="box-about-home lazy" data-src="<?=base_url('img/bg-about.jpg');?>">
  <div class="grid-container bg-w box-in" >
    <div class="grid-x grid-padding-x ">
      <div class="cell small-12 text-center">
        <h3><b><?=$a->title_1;?></b></h3>
      </div>
    </div>
    <div class="grid-x grid-padding-x ">
      <div class="cell small-12 medium-8 medium-offset-2 text-center">
        <div class="">
          <h4 class="c-cm" ><b><?=$a->title_2;?></b> </h4>
          <?=$a->intro_detail;?>
        </div>
        <br/>
        <br/>
        <a href="<?=site_url('about_us');?>" class="button large">อ่านต่อ</a>   
      </div>
    </div>
  </div>
</section>

<?php 
$n = $this->db->order_by('id','desc')->limit(2)->get_where('news',array('status'=>'open'))->result();
?>

<section class="bg-ct">
  <div class="grid-container bg-w box-in" >
    <div class="grid-x grid-padding-x ">
      <div class="cell small-12 text-center">
        <h3 class="c-cm"><b>ข่าวภายในสำนักงาน</b></h3>
        <br/>
      </div>
    </div>

    <div class="grid-x grid-margin-x grid-padding-x">
      <div class="cell small-12 medium-offset-1 medium-10 ">
        <div class="grid-x grid-margin-x grid-padding-x">
<?php if($n!=false): ?>
  <?php foreach($n as $nn):
    
  $str = strip_tags($nn->detail);
  $string =  iconv_substr($str,0,125, "UTF-8");
    
    ?>
          <div class="cell small-6 i-inews">
            <div class="pic">
              <img src="<?=base_url('img/news/'.$nn->picture);?>" alt="<?=$nn->title;?>">
            </div>
            <div class="txt">
              <h4><?=$nn->title;?></h4>

              <?=$string;?>...
              <br/><br/>
              <span><img src="<?=base_url('img/view.svg');?>" width="20"> เข้าอ่าน &nbsp;<?=$nn->view;?> </span>
            </div>
            <div class="clearfix foot-n">
              <div class="float-left date-n"><?=date('d/m/Y',strtotime($nn->datetime));?></div>
              <a href="<?=site_url('news/detail/'.$nn->id.'/'.urldecode($nn->title));?>" class="button float-right">อ่านต่อ</a>
            </div>
          </div>
<?php endforeach; ?>
<?php endif; ?>
        </div>
      </div>
    </div>

      <div class="grid-x grid-padding-x ">
          <div class="cell small-12 text-center"> 
 

     <a href="<?=site_url('news');?>" class="more-news">ดูข่าวทั้งหมด</a>

          </div>
      </div>
  </div>
</section>



<?php 

$pp = $this->db->order_by('id','desc')->limit(10)->get_where('doc',array('status !='=>'close'))->result();

?>
<section class="bg-cs">
<div class="grid-container box-in" >
    <div class="grid-x grid-padding-x">
      <div class="cell small-12 ">
      <h3 class="text-center c-w"><b>เอกสารเผยแพร่</b></h3>
      </div>
<?php if($pp!=false): ?>
<div class="cell small-12 medium-10 medium-offset-1">
<div class="posi-re">
<div id="goToPrevSlide">&#x276E;</div>
<div id="goToNextSlide">&#x276F;</div>
 <ul id="light-slider" class="cs-hidden">

<?php foreach($pp as $p): ?>
    <li>
       <a target="_blank" href="<?=site_url('file/doc/'.$p->file);?>">
        <span class="icon-pdf"></span>
        <div class="text-ls">
         <?=$p->name;?>
        </div>
       </a>
    </li>
  <?php endforeach; ?>

</ul>
</div>
 </div>
  <?php endif; ?>


    </div>

      <div class="grid-x grid-padding-x grid-margin-x">
          <div class="cell small-12 text-center"> 
 
     <br/>
     <a href="<?=site_url('doc_public');?>" class="more-news doc-link">อ่านทั้งหมด</a>
     <br/>
          </div>
      </div>

  </div>     
</section>
<?php       
     $this->db->select_sum('NUM');          
     $re = $this->db->get('daily')->row();  
     
     	// This Month //
       $strSQL = " SELECT SUM(NUM) AS CountMonth FROM daily WHERE DATE_FORMAT(DATE,'%Y-%m')  = '".date('Y-m')."' ";
       $objQuery = $this->db->query($strSQL);
       $objResult = $objQuery->row();
       $strThisMonth = $objResult->CountMonth;
    
?>
<section class="box-visit-home lazy" data-src="<?=base_url('img/bg-fruit.jpg');?>">
<div class="grid-container box-in" >
    <div class="grid-x">
      <div class="cell small-12">
       <div class="sec-top">
         <a href="tel:1569" class="call-center">
         <img src="<?=base_url('img/tel.svg');?>" width="40"> &nbsp;สายด่วน  1596
         </a>
<br/><br/><br/><br/>
<div class="grid-x grid-margin-x grid-padding-x text-center">
  <div class="cell shrink i-vi">
   <div><?=str_pad($this->visit_model->getAmountVisitors(),6,"0",STR_PAD_LEFT);?></div>
  จำนวนผู้ชมขณะนี้
  </div>
  <div class="cell shrink i-vi">
   <div><?=str_pad($strThisMonth,6,"0",STR_PAD_LEFT);?></div>
   จำนวนผู้ชมเดือนนี้
  </div>
  <div class="cell shrink i-vi">
   <div><?=str_pad($re->NUM,6,"0",STR_PAD_LEFT);?></div>
   จำนวนผู้ชมทั้งหมด
  </div>
</div>

       </div>
<?php
$ll=$this->db->order_by('order_number','asc')->get('link_web')->result();
?>
<?php if($ll!=false): ?>
<div class="sec-bottom">
  <h3>เชื่อมโยงไปยังเว็บไซต์</h3>
<hr>

<div class="text-center link-img">
<?php foreach($ll as $l): ?>
<a href="<?=$l->link;?>" target="_blank"><img src="<?=base_url('img/'.$l->picture);?>"></a>&nbsp;
<?php endforeach;?>
</div>

<?php endif; ?>

</div>
      </div>
    </div>
  </div>
</section>
<?php
$g = $this->db->get('contact')->row();
$ss = $this->db->get('contact_section')->result();

?>

<section class="h-page lazy" data-src="<?=base_url('img/bg-h1.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
        <img src="<?=base_url('img/contact-logo.png');?>" class="float-right">
      </div>
    </div>
  </div>
</section>

<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1>ติดต่อเรา</h1>
         <hr>
      </div>
    </div>
  </div>
</section>


<div class="grid-container">
   <div class="grid-x">
     <div class="cell small-12">
     <div id="map"></div>
            <script>
               var map;
               function initMap() {
                var styledMapType = new google.maps.StyledMapType(
            [
              {elementType: 'geometry', stylers: [{color: '#A6BC95'}]},
              {elementType: 'labels.text.fill', stylers: [{color: '#523735'}]},
              {elementType: 'labels.text.stroke', stylers: [{color: '#f5f1e6'}]},
              {
                featureType: 'administrative',
                elementType: 'geometry.stroke',
                stylers: [{color: '#c9b2a6'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'geometry.stroke',
                stylers: [{color: '#dcd2be'}]
              },
              {
                featureType: 'administrative.land_parcel',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ae9e90'}]
              },
              {
                featureType: 'landscape.natural',
                elementType: 'geometry',
                stylers: [{color: '#69AD88'}]
              },
              {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#93817c'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'geometry.fill',
                stylers: [{color: '#a5b076'}]
              },
              {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#447530'}]
              },
              {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#f5f1e6'}]
              },
              {
                featureType: 'road.arterial',
                elementType: 'geometry',
                stylers: [{color: '#fdfcf8'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#f8c967'}]
              },
              {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#e9bc62'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry',
                stylers: [{color: '#e98d58'}]
              },
              {
                featureType: 'road.highway.controlled_access',
                elementType: 'geometry.stroke',
                stylers: [{color: '#db8555'}]
              },
              {
                featureType: 'road.local',
                elementType: 'labels.text.fill',
                stylers: [{color: '#806b63'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.fill',
                stylers: [{color: '#8f7d77'}]
              },
              {
                featureType: 'transit.line',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#ebe3cd'}]
              },
              {
                featureType: 'transit.station',
                elementType: 'geometry',
                stylers: [{color: '#dfd2ae'}]
              },
              {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [{color: '#b9d3c2'}]
              },
              {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#92998d'}]
              }
            ],
            {name: 'New Styled Map'});
                 map = new google.maps.Map(document.getElementById('map'), {
                   center: {lat: <?=$g->lat;?>, lng:<?=$g->long;?>},
                   zoom: 11,
                   mapTypeControl:false,
                   streetViewControl:false
                 });

var marker = new google.maps.Marker({
	   position: new google.maps.LatLng(13.8826,100.486106),
	   map: map,
	});


    var info = new google.maps.InfoWindow({
		content : '<div class="gg"><h1>กรมการค้าภายใน</h1><div class="gg-address">563 ถนนนนทบุรี ตำบลบางกระสอ อำเภอเมือง <br/>จังหวัดนนทบุรี  11000 <br/><br/><b> โทรศัพท์ </b>: 0-2507-5530 , สายด่วน 1569<br/><b>อีเมล</b> :  webmaster@dit.go.th<br/><br/></div></div>'
	});

	google.maps.event.addListener(marker, 'click', function() {
		info.open(map, marker);
	});
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');
        info.open(map, marker);
               }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGnE7yUytjE4zYESPpKBEAETMBPD6LgDM&callback=initMap"
               async defer></script>
      </div>
    </div>
</div>


<section class="box-contact">
<div class="grid-container">



   <div class="grid-x">
     <div class="cell small-12">
       <h3>ติดต่อกรมการค้าภายใน</h3>
       <hr/>
      </div>
      <div class="cell small-12">
      <?=$g->address;?>
      <br/><br/>
      </div>
    </div>


<?php if($ss!=false):?>

   <?php foreach($ss as $s): 
          $sub =$this->db->get_where('sub_department',array('id'=>$s->id))->result();
    
    ?>
   <div class="grid-x">
     <div class="cell small-12">
       <h3><?=$s->department;?></h3>
       <hr/>
      </div>
  </div>
    
  <div class="grid-x r-t">
     <div class="cell small-1">
    ลำดับ
    </div>
    <div class="cell small-5">
    หน่วยงาน
    </div>
    <div class="cell small-3">
    โทรศัพท์ / โทรสาร
    </div>
    <div class="cell small-3">
    หมายเลยภายใน
    </div>
</div>

        <?php if($sub!=false): ?>
        <?php $i =1; foreach($sub as $r):  ?>


<div class="grid-x r-t">
     <div class="cell small-1">
    <?=$i++;?>
    </div>
    <div class="cell small-5">
    <?=$r->sub_department_name;?>
    </div>
    <div class="cell small-3">
    <?=$r->sub_department_tel;?>
    </div>
    <div class="cell small-3">
    <?=$r->sub_department_inner;?>
    </div>
</div>
   
        <?php endforeach; ?>
        <?php endif;?>
   <?php endforeach; ?>
<?php endif;?>
  

</div>
</section>

<?=$this->load->view('link_web');?>

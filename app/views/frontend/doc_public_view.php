<section class="h-page lazy" data-src="<?=base_url('img/bg-h2.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12 logo-com">
        <img src="<?=base_url('img/doc-logo.png');?>" class="float-right" style="max-width:500px;">
      </div>
    </div>
  </div>
</section>

<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1>เอกสารเผยแพร่ <div class="back"> << ย้อนกลับ</div></h1>
      </div>
    </div>
    <div class="row align-justify">
  <div class="column small-12 medium-6" style="padding:0;">เอกสารทั้งหมด : <?=$count;?></div>
  <div class="column small-12 medium-3" style="padding:0;"><?=$this->pagination->create_links();?></div>
</div>
<div class="grid-x">
<div class="cell small-12">
         <hr/>
</div>
</div>


  </div>
</section>

<section class="list-pdf">
<div class="grid-container" >
<div class="grid-x grid-margin-x small-up-2 medium-up-4 large-up-5">

<?php if($n!=false):  ?> 
<?php foreach($n as $r): ?>
  <div class="cell">
  <a target="_blank" href="<?=site_url('file/doc/'.$r->file);?>">
      <span class="icon-pdf"></span>
        <div class="text-ls">
        <?=$r->name;?>
        </div>
   </a>
  </div>
  <?php endforeach; ?>
<?php endif;?>


</div>
</div>
</section>

<?=$this->load->view('link_web');?>
<section class="h-page lazy" data-src="<?=base_url('img/bg-h1.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
        <img src="<?=base_url('img/news-logo.png');?>" class="float-right">
      </div>
    </div>
  </div>
</section>
<?php 
$g=$this->db->get_where('news',array('id'=>$id,'status !='=>'close'))->row();
if($g==false):
redirect('news');
endif;

$gal=$this->db->get_where('gallery_news',array('id'=>$id))->result();
?>
<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1><?=$g->title;?> <div class="back"> << ย้อนกลับ</div></h1>
         <div class="">วันที่เขียนข่าว : <?=date('d/m/Y',strtotime($g->datetime));?><br/><span>  จำนวนผู้เข้าชม : <img src="<?=base_url('img/view.svg');?>" width="20"> &nbsp;<?=$g->view;?> </span></div>
         <hr>
      </div>
    </div>
  </div>
</section>


<section class="news-detail">
<div class="grid-container" >
    <div class="grid-x grid-margin-x">
        <div class="cell small-12 medium-8 medium-offset-2">
        <ul id="imageGallery">

  <?php if($g->picture!=""): ?>
      <li data-thumb="<?=base_url('img/news/'.$g->picture);?>" data-src="<?=base_url('img/news/'.$g->picture);?>">
        <img src="<?=base_url('img/news/'.$g->picture);?>" />
      </li>
   <?php endif;?>

   <?php if($gal!=false): ?>
    <?php foreach($gal as $ga): ?>
    <li data-thumb="<?=base_url('img/news/'.$ga->url);?>" data-src="<?=base_url('img/news/'.$ga->url);?>">
        <img src="<?=base_url('img/news/'.$ga->url);?>" />
    </li>
    <?php endforeach;?>
   <?php endif;?>

    </ul>

        </div>
    </div>
  </div>


<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5b71aa2504b9a500117b0fea&product=inline-share-buttons' async='async'></script>
<div class="grid-container bg-gray box-detail-news" >
    <div class="grid-x grid-margin-x">
        <div class="cell small-12">
        <div class="sharethis-inline-share-buttons"></div>
        <hr/>

        </div>
        <div class="cell small-12 de-box">
        <?=$g->detail;?>
<br/><br/>
<span class="re-i c-ct">ที่มาของข่าว : <?=$g->ref;?></span>
<br/>
        </div>
    </div>
  </div>
</section>

<?=$this->load->view('link_web');?>
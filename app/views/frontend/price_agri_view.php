<?php
$g=$this->db->get_where('price_agri',array('id'=>$id))->row();
if($g==false):
redirect('price_agri');
endif;
?>
<section class="h-page lazy" data-src="<?=base_url('img/bg-h1.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12 logo-com">
        <img src="<?=base_url('img/agri-logo.png');?>" class="float-right" style="max-width:500px;">
      </div>
    </div>
  </div>
</section>
<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1><?=$g->name;?> <div class="back"> << ย้อนกลับ</div></h1>
      </div>
    </div>
    <div class="row align-justify">
  <div class="column small-12 medium-6" style="padding:0;">ทั้งหมด : <?=$count==0?'-':$count;?></div>
  <div class="column small-12 medium-3" style="padding:0;">
  <?php if($count!=0):echo $this->pagination->create_links();endif;?>
  </div>
</div>
<div class="grid-x">
<div class="cell small-12">
         <hr/>
</div>
</div>

  </div>
</section>

<section class="row-ta">
<div class="grid-container" >


<?php if($count!=0): ?>
<?php foreach($pp as $p): ?>
<a target="_blank" href="<?=base_url('file/price/'.$p->file);?>" class="grid-x grid-padding-x">
      <div class="cell small-10">
      <?=$p->file_name;?>
      </div>
      <div class="cell small-2">
          <img src="<?=base_url('img/i-pdf.svg');?>" width="25">
      </div>
</a>
<?php endforeach;?>
<?php else: ?>

<div class="grid-x grid-padding-x">
      <div class="cell small-12 text-center">
          <br> <br> <br>
         --- ไม่พบข้อมูล ---
          <br> <br> <br>
      </div>
</div>


<?php endif;?>

</div>
</section>

<?=$this->load->view('link_web');?>
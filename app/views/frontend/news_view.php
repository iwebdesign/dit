<section class="h-page lazy" data-src="<?=base_url('img/bg-h1.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
        <img src="<?=base_url('img/news-logo.png');?>" class="float-right">
      </div>
    </div>
  </div>
</section>

<section class="page-about">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12">
         <h1>ข่าวภายในสำนักงาน</h1>
      </div>
    </div>
<div class="row align-justify">
  <div class="column small-12 medium-6" style="padding:0;">ข่าวทั้งหมด : <?=$count;?></div>
  <div class="column small-12 medium-3" style="padding:0;"><?=$this->pagination->create_links();?></div>
</div>
<div class="grid-x">
<div class="cell small-12">
         <hr/>
</div>
</div>
  </div>
</section>

<section class="news-all">
<div class="grid-container" >
    <div class="grid-x grid-margin-x">

<?php if($n!=false):  ?>    

     <?php foreach($n as $r): 
      
      $str = strip_tags($r->detail);
      $string =  iconv_substr($str,0,125, "UTF-8");
      ?>
      <div class="cell small-6 medium-4 i-inews">
            <div class="pic">
            <img src="<?=base_url('img/news/'.$r->picture);?>" alt="<?=$r->title;?>">
            </div>
            <div class="txt">
              <h4><?=$r->title;?></h4>
              <?=$string;?>...
              <br/><br/>
              <span><img src="<?=base_url('img/view.svg');?>" width="20"> เข้าอ่าน &nbsp;<?=$r->view;?> </span>
            </div>
            <div class="clearfix foot-n">
              <div class="float-left date-n"><?=date('d/m/Y',strtotime($r->datetime));?></div>
              <a href="<?=site_url('news/detail/'.$r->id.'/'.urldecode($r->title));?>" class="button float-right">อ่านต่อ</a>
            </div>
      </div>

<?php endforeach; ?>
<?php endif; ?>

    </div>
  </div>
</section>

<?=$this->load->view('link_web');?>
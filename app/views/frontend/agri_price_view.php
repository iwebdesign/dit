<section class="h-page lazy" data-src="<?=base_url('img/bg-h1.jpg');?>">
<div class="grid-container" >
    <div class="grid-x">
      <div class="cell small-12 logo-com">
        <img src="<?=base_url('img/agri-logo.png');?>" class="float-right" style="max-width:500px;">
      </div>
    </div>
  </div>
</section>
<?php
$thaiweek=array("วันอาทิตย์","วันจันทร์","วันอังคาร","วันพุธ","วันพฤหัส","วันศุกร์","วันเสาร์");
$thaimonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); 
?>
<section class="page-about"> <div class="grid-container"> <div class="grid-x"> <div class="cell small-12 text-center"> <h1>รายงานการติดตามภาวะตลาดสินค้าในกรุงเทพมหานคร </h1> 
<h4>ประจำวัน<?=$thaiweek[date("w")] ,"ที่",date(" j "), $thaimonth[date(" m ")-1] , " พ.ศ. ",date(" Y ")+543; ?> เวลา 9.00 - 12.00 น.</h4>
<hr> </div> </div> </div> </section>


<section>
   <div class="grid-container">
   
   <div class="grid-x">
   
   <table class="radius bordered shadow">
   
    <tr class="text-center" style="background-color: #f1f1f1;">
        <td rowspan="2">ลำดับที่</td>
        <td rowspan="2" width="350">รายการ</td>
        <td colspan="2">ราคาขายส่ง</td>
        <td colspan="2">ราคาขายปลีก</td>
        <td rowspan="2" width="270">รายละเอียด</td>
    </tr>
    <tr class="text-center">
        <td>ต่ำสุด</td>
        <td>สูงสุด</td>
  
        <td>ต่ำสุด</td>
        <td>สูงสุด</td>
    </tr>

 <?php

 if(count($r)>0):
for ($x = 0; $x < count($r); $x++) { ?>
    <tr>
        <td class="text-center"><?=$x+1;?></td>
        <td><?=$r[$x]['name'];?></td>
        <td class="text-center"><?=isset($r[$x]['wholesaleLow'])?$r[$x]['wholesaleLow']:'-';?></td>
        <td class="text-center"><?=isset($r[$x]['wholesaleHigh'])?$r[$x]['wholesaleHigh']:'-';?></td>
        <td class="text-center"><?=isset($r[$x]['retailLow'])?$r[$x]['retailLow']:'-';?></td>
        <td class="text-center"><?=isset($r[$x]['retailHight'])?$r[$x]['retailHight']:'-';?></td>
        <td><?=isset($r[$x]['note'])?$r[$x]['note']:'-';?></td>
    </tr>
<?php } 
else:
?>

<tr> <td colspan="7" class="text-center"> <br/>-- ไม่พบข้อมูล -- <br/></td> </tr>
<?php
endif;
?> 



</table>


   </div>
   
   </div>

</section>

<?=$this->load->view('link_web');?>
<?php 

class Visit_micro_model extends CI_Model {
    var $sessionTimeInMin = 5;
  function __construct()
   {
    parent::__construct();
   }

   public  function VisitorCounter($mi)
   {
       $ip = $_SERVER['REMOTE_ADDR'];
       $this->cleanVisitors($mi);

     if ($this->visitorExists($ip,$mi))
       {
           $this->updateVisitor($ip,$mi);
       } else
       {
           $this->addVisitor($ip,$mi);
       }
   }

   private  function cleanVisitors($mi=null)
      {
 
          $sql = "select * from  online_micro where micro_id = '$mi' ";
          $res = $this->db->query($sql)->result();
          foreach($res as $row)
          {
              $ro = $row->id;
              if (time() - $row->lastvisit >= $this->sessionTimeInMin * 60)
              {
                  $dsql = "delete from online_micro where id = $ro";
                  $this->db->query($dsql);
              }
          }
      }

      public function visitorExists($ip,$mi)
      {
          $sql = "select * from online_micro where ip = '$ip' and micro_id = '$mi'";
          $res = $this->db->query($sql)->num_rows();
          if ($res > 0)
          {
              return true;
          } else
              if ($res == 0)
              {
                  return false;
              }
      }

      private  function updateVisitor($ip,$mi)
    {
 
        $sql = "update online_micro set lastvisit = '" . time() . "' where ip = '$ip' and micro_id = '$mi'";
        $this->db->query($sql);
    }

    private  function addVisitor($ip,$mi)
    {
        $sql = "insert into online_micro (ip ,lastvisit,micro_id) value('$ip', '" . time() . "','$mi')";
        $this->db->query($sql);
    }

    public  function getAmountVisitors($mi)
    {
        $sql = "select count(id) as num from online_micro where micro_id= '$mi'";
        $res = $this->db->query($sql);
        $row = $res->row();
        return $row->num;
    }

    // public function show()
    // {
    //     echo '<div style="padding:5px; margin:auto; background-color:#fff"><b>' .
    //         $this->getAmountVisitors() . 'visitors online</b></div>';
    // }


}
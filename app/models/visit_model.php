<?php 

class Visit_model extends CI_Model {
    var $sessionTimeInMin = 5;
  function __construct()
   {
    parent::__construct();
   }

   public  function VisitorCounter()
   {
       $ip = $_SERVER['REMOTE_ADDR'];
       $this->cleanVisitors();

     if ($this->visitorExists($ip))
       {
           $this->updateVisitor($ip);
       } else
       {
           $this->addVisitor($ip);
       }
   }

   private  function cleanVisitors()
      {
 
          $sql = "select * from  online";
          $res = $this->db->query($sql)->result();
          foreach($res as $row)
          {
              $ro = $row->id;
              if (time() - $row->lastvisit >= $this->sessionTimeInMin * 60)
              {
                  $dsql = "delete from online where id = $ro";
                  $this->db->query($dsql);
              }
          }
      }

      public function visitorExists($ip)
      {
          $sql = "select * from online where ip = '$ip'";
          $res = $this->db->query($sql)->num_rows();
          if ($res > 0)
          {
              return true;
          } else
              if ($res == 0)
              {
                  return false;
              }
      }

      private  function updateVisitor($ip)
    {
 
        $sql = "update online set lastvisit = '" . time() . "' where ip = '$ip'";
        $this->db->query($sql);
    }

    private  function addVisitor($ip)
    {
        $sql = "insert into online (ip ,lastvisit) value('$ip', '" . time() . "')";
        $this->db->query($sql);
    }

    public  function getAmountVisitors()
    {
        $sql = "select count(id) as num from online";
        $res = $this->db->query($sql);
        $row = $res->row();
        return $row->num;
    }

    // public function show()
    // {
    //     echo '<div style="padding:5px; margin:auto; background-color:#fff"><b>' .
    //         $this->getAmountVisitors() . 'visitors online</b></div>';
    // }


}
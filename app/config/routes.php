<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['default_controller'] = "home";
$route['404_override'] = '';
$route['home/(:any)'] = 'home/index/$1';
$route['department/(:any)'] = 'microsite/index/$1';
$route['department_news/(:any)'] = 'department_news/index/$1';
$route['department_news_detail/(:any)'] = 'department_news_detail/index/$1';
$route['department_calendar/(:any)'] = 'department_calendar/index/$1';
$route['department_personnel/(:any)'] = 'department_personnel/index/$1';
$route['department_doc/(:any)'] = 'department_doc/index/$1';
$route['agri_price/(:any)'] = 'agri_price/index/$1';
/* End of file routes.php */
/* Location: ./application/config/routes.php */
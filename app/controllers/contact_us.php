<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	public function index()
	{
		$data = array('content'=>'contact_us_view');
		$this->load->view('layout/template',$data);
	}
}

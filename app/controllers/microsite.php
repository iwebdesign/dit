<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Microsite extends CI_Controller {
    function __construct()
    {
     parent::__construct();
     $this->load->model('visit_micro_model');
    }
 
	public function index($id=null,$name=null,$year=null,$month=null)
	{
		$prefs['template'] = '

    

        {heading_row_start}{/heading_row_start}
		heading_title_cell}<h4>{heading}</h4>{/heading_title_cell}
        {heading_previous_cell}<div class="text-left prev-calen"><a href="{previous_url}">&lt;&lt;<font>เดือนก่อนหน้า</font></a></div>{/heading_previous_cell}
        {heading_next_cell}<div class="text-right next-calen"><a href="{next_url}"><font>เดือนถัดไป</font>&gt;&gt;</a></div>{/heading_next_cell}

		{heading_row_end}{/heading_row_end}
		

		{table_open}<table border="0" class="calen" cellpadding="0" cellspacing="0">{/table_open}
        {week_row_start}<tr class="week">{/week_row_start}
        {week_day_cell}<td>{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}

        {cal_row_start}<tr>{/cal_row_start}
        {cal_cell_start}<td>{/cal_cell_start}
        {cal_cell_start_today}<td>{/cal_cell_start_today}
        {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

        {cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
        {cal_cell_content_today}<div class="highlight"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

        {cal_cell_no_content}{day}{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

        {cal_cell_blank}&nbsp;{/cal_cell_blank}

        {cal_cell_other}{day}{/cal_cel_other}

        {cal_cell_end}</td>{/cal_cell_end}
        {cal_cell_end_today}</td>{/cal_cell_end_today}
        {cal_cell_end_other}</td>{/cal_cell_end_other}
        {cal_row_end}</tr>{/cal_row_end}

        {table_close}</table>{/table_close}
';
$prefs['show_next_prev'] = TRUE;
$prefs['next_prev_url']  = site_url('department/'.$id.'/'.$name.'/');
$prefs['day_type'] = 'short';

$this->load->library('calendar', $prefs);
        $data = array('content'=>'home_view','id'=>$id,'funjs'=>'fun_home_micro');
		$this->load->view('layout/template_micro',$data);
	}

	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_dit extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->library('image_CRUD');
		if (!isset($this->session->userdata['validated'])) {
          redirect('/login');
		}

	}
	
    public function _iweb_output($output = null)
	{
		$this->load->view('admin/template.php',$output);
	}

	public function _iframe_output($output = null)
	{
		$this->load->view('admin/template_iframe.php',$output);
	}
	
	function _gal_output($output = null)
	{
		$this->load->view('example-pic.php',$output);	
	}

	function _gal_micro_output($output = null)
	{
		$this->load->view('example-pic-micro.php',$output);	
	}
    
    public function index()
	{
	//	$this->_iweb_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		$crud = new grocery_CRUD();
		$crud->set_table('dashboard');
		$output = $crud->render();
		$this->_iweb_output($output);
	}
		

	public function users()
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('users');
		$crud->field_type('password', 'password');
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
$crud->callback_before_update(array($this,'encrypt_password_callback'));
$crud->callback_edit_field('password',array($this,'decrypt_password_callback'));
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}


function encrypt_password_callback($post_array, $primary_key = null)
{
$this->load->library('encrypt');
 
$key = 'super-secret-key';
$post_array['password'] = $this->encrypt->encode($post_array['password'], $key);
return $post_array;
}
 
function decrypt_password_callback($value)
{
$this->load->library('encrypt');
 
$key = 'super-secret-key';
$decrypted_password = $this->encrypt->decode($value, $key);
return "<input type='password' name='password' value='$decrypted_password' />";
}

public function setting()
{
  $crud = new grocery_CRUD();
	$crud->set_table('setting');
	$crud->unset_add();
	$crud->unset_export();
	$crud->unset_print();
	$output = $crud->render();
	$this->_iweb_output($output);
}

public function social()
{
  $crud = new grocery_CRUD();
	$crud->set_table('social');
	//$crud->unset_add();
	$crud->unset_export();
	$crud->unset_print();
	$output = $crud->render();
	$this->_iweb_output($output);
}

public function intro()
{
  $crud = new grocery_CRUD();
	$crud->set_table('intro');
    $crud->unset_add();
   // $crud->order_by('id','desc');
    $crud->set_field_upload('picture','img');
	$crud->unset_export();
	$crud->unset_print();
	$crud->unset_delete();
	$crud->unset_read();
	$output = $crud->render();
	$this->_iweb_output($output);
}



	public function slide_home()
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('slide_home');
		$crud->set_field_upload('picture','img/slide-home');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function about()
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('about');
		$crud->unset_add();
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}
	public function personnel()
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('personnel');
		$crud->set_field_upload('picture','img/personnel');
	//	$crud->unset_add();
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}
		
	public function complaint()
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('complaint');
		$crud->unset_add();
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}
	public function price_agri()
	{
  	   $crud = new grocery_CRUD();
		$crud->set_table('price_agri');
		$crud->set_field_upload('picture','img/price');
		$crud->columns('picture','name','status','file');
		$crud->callback_column('file',array($this,'_callback_price'));
		$crud->callback_after_delete(array($this,'delete_file'));
		$crud->unset_read();
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function price_agri_file($id)
	{
		$crud = new grocery_CRUD();
		$crud->set_table('price_agri_file');
		$crud->where('id',$id);
		$crud->field_type('id', 'hidden', $id);
		$crud->set_field_upload('file','file/price');
		$crud->unset_columns('id');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iframe_output($output);
	}


	public function _callback_price($value, $row)
{
return "<a class='btn btn-info' data-fancybox data-type='iframe' data-src='".site_url('admin_dit/price_agri_file/'.$row->id)."' href='javascript:;'>+ รายการเอกสาร</a>";
}

public function delete_file($primary_key)
{
$this->load->helper('file');
$gg = $this->db->get_where('price_agri_file', array('id' => $primary_key))->result();
if($gg!=false):

	foreach($gg as $g):
		unlink('./file/price/'.$g->file);
	endforeach;

return	$this->db->delete('price_agri_file', array('id' => $primary_key));

endif;

}


function iweb_callback_after_upload($uploader_response,$field_info, $files_to_upload)
{
$this->load->library('image_moo');
 
//Is only one file uploaded so it ok to use it with $uploader_response[0].
$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
 
$this->image_moo->load($file_uploaded)->resize(800,600)->save($file_uploaded,true);
 
return true;
}
	public function document()
	{
  	   $crud = new grocery_CRUD();
		$crud->set_table('doc');
		$crud->set_field_upload('file','file/doc');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function contact()
	{
  	   $crud = new grocery_CRUD();
		$crud->set_table('contact');
	     $crud->unset_add();
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function contact_section()
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('contact_section');
		$crud->add_action('รายชื่อผู้ติดต่อ', '', 'admin_dit/contact_department','fa-plus');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function contact_department($id=null)
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('sub_department');
		$crud->where('id',$id);
		$crud->field_type('id', 'hidden', $id);
		$crud->unset_columns('id');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function news()
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('news');
		$crud->order_by('id','desc');
		$crud->set_field_upload('picture','img/news');
		$crud->display_as('picture','Picture <br/> (907 × 587 px)');
		$crud->add_action('แกลลอรี่รูป', '', 'admin_dit/gallery_news','fa-plus');
		$crud->unset_columns('detail');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	function gallery_news()
	{
		$image_crud = new image_CRUD();
	
		$image_crud->set_primary_key_field('gal_id');
		$image_crud->set_url_field('url');
		$image_crud->set_table('gallery_news')
		->set_relation_field('id')
		->set_ordering_field('priority')
		->set_image_path('img/news');
			
		$output = $image_crud->render();
	
		$this->_gal_output($output);
	}

	public function link_relate()
	{
     	$crud = new grocery_CRUD();
		$crud->set_table('link_relate');
		$crud->set_field_upload('picture','img/');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function link_web()
	{
     	$crud = new grocery_CRUD();
		$crud->set_table('link_web');
		$crud->set_field_upload('picture','img/');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function microsite()
	{
		 $crud = new grocery_CRUD();
		$crud->set_table('microsite');
		
	//	$crud->set_field_upload('micro_picture','img/micro');
		$crud->add_action('สไลด์', '', 'admin_dit/slide_micro','fa-plus');
		$crud->add_action('ผู้รับผิดชอบ', '', 'admin_dit/personnel_micro','fa-plus');
		$crud->add_action('ข้อมูลเผยแพร่', '', 'admin_dit/doc_micro','fa-plus');
		$crud->add_action('ปฎิทินกิจกรรม', '', 'admin_dit/calendar_micro','fa-plus');
		$crud->add_action('ข่าวสาร', '', 'admin_dit/news_micro','fa-plus');
		$crud->add_action('เชื่อมโยงเว็บ', '', 'admin_dit/link_web_micro','fa-plus');
		$crud->unset_fields('micro_picture');
		$crud->unset_columns('micro_picture');
		$crud->unset_export();
		$crud->unset_print();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		$crud->unset_read();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function slide_micro($id=null)
	{
     	$crud = new grocery_CRUD();
		$crud->set_table('slide_micro');
		$crud->where('micro_id',$id);
		$crud->field_type('micro_id', 'hidden', $id);
		$crud->set_field_upload('slide_micro_picture','img/micro/slide');
		$crud->unset_columns('micro_id');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function personnel_micro($id=null)
	{
     	$crud = new grocery_CRUD();
		$crud->set_table('personnel_micro');
		$crud->where('micro_id',$id);
		$crud->field_type('micro_id', 'hidden', $id);
		$crud->set_field_upload('picture','img/micro/personnel');
		$crud->unset_columns('micro_id');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function calendar_micro($id=null)
	{
     	$crud = new grocery_CRUD();
		$crud->set_table('calendar_micro');
		$crud->where('micro_id',$id);
		$crud->field_type('micro_id', 'hidden', $id);
		$crud->unset_columns('micro_id');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}


	public function doc_micro($id=null)
	{
     	$crud = new grocery_CRUD();
		$crud->set_table('doc_micro');
		$crud->where('micro_id',$id);
		$crud->field_type('micro_id', 'hidden', $id);
		$crud->add_action('ไฟล์เอกสาร', '', 'admin_dit/doc_micro_file','fa-plus');
		$crud->unset_columns('micro_id');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function doc_micro_file($id=null)
	{
     	$crud = new grocery_CRUD();
		$crud->set_table('doc_micro_file');
		$crud->where('id',$id);
		$crud->field_type('id', 'hidden', $id);
		$crud->set_field_upload('file','file/micro');
		$crud->unset_columns('id');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	public function news_micro($id=null)
	{
  	$crud = new grocery_CRUD();
		$crud->set_table('news_micro');
		$crud->order_by('id','desc');
		$crud->where('micro_id',$id);
		$crud->field_type('micro_id', 'hidden', $id);
		$crud->set_field_upload('picture','img/micro/news');
		$crud->display_as('picture','Picture <br/> (907 × 587 px)');
		$crud->add_action('แกลลอรี่รูป', '', 'admin_dit/gallery_news_micro','fa-plus');
		$crud->unset_columns('detail');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}

	function gallery_news_micro()
	{
		$image_crud = new image_CRUD();
	
		$image_crud->set_primary_key_field('gal_id');
		$image_crud->set_url_field('url');
		$image_crud->set_table('gallery_news_micro')
		->set_relation_field('id')
		->set_ordering_field('priority')
		->set_image_path('img/micro/news');
			
		$output = $image_crud->render();
	
		$this->_gal_micro_output($output);
	}

	public function link_web_micro($id=null)
	{
     	$crud = new grocery_CRUD();
		$crud->set_table('link_micro');
		$crud->where('micro_id',$id);
		$crud->field_type('micro_id', 'hidden', $id);
		$crud->set_field_upload('picture','img/micro/news');
		$crud->unset_columns('micro_id');
		$crud->unset_export();
		$crud->unset_print();
		$output = $crud->render();
		$this->_iweb_output($output);
	}


		

}
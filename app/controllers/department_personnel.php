<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_personnel extends CI_Controller {
    function __construct()
    {
     parent::__construct();
     $this->load->model('visit_micro_model');
    }
 
	public function index($id=null,$name=null)
	{
        $data = array('content'=>'personnel_view','id'=>$id,'funjs'=>'fun_home_micro');
		$this->load->view('layout/template_micro',$data);
	}

	
}
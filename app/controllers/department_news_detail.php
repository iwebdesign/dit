<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_news_detail extends CI_Controller {
    function __construct()
    {
     parent::__construct();
     $this->load->model('visit_micro_model');
    }
 
	public function index($id=null,$slug=null,$nid=null)
	{
        if($id == "" || $slug =="" || $nid ==""):
            redirect('/');
            exit();
            endif;
            $this->db->select('*');
            $this->db->from('news_micro');
            $this->db->where('id',$nid);
            $this->db->where('micro_id',$id);
            $row=$this->db->get()->row();
            $vi = array(
                'view' => $row->view+1
             );
        
             $this->db->where('id', $nid);
             $this->db->update('news_micro', $vi); 
            $data = array('content'=>'news_detail_view','id'=>$id,'nid'=>$nid);
            $this->load->view('layout/template_micro',$data);
	}
	
}

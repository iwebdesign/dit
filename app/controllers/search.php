<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {
	public function index()
	{
		$data = array('content'=>'search_view');
		$this->load->view('layout/template',$data);
	}
}
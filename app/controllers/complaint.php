<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Complaint extends CI_Controller {

	public function index()
	{
        $data = array('content'=>'complaint_view','funjs'=>'fun_complaint');
		$this->load->view('layout/template',$data);
	}
}
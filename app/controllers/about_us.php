<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About_us extends CI_Controller {

	public function index()
	{
        $data = array('content'=>'about_us_view');
		$this->load->view('layout/template',$data);
	}
}

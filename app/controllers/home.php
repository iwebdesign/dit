<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index($page=null)
	{
		$this->db->select('status')->from('intro')->where('status !=','close');
		$g=$this->db->get()->row();

		if($g!=false && $this->uri->segment(1)==false){
			redirect('/intro');
		}

        $data = array('content'=>'home_view');
		$this->load->view('layout/template',$data);
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mailer extends CI_Controller {

	public function __construct(){
			parent::__construct();

		}

	public function index()
	{
    $data = array(
        'menu'    => '6',
        'header'  => 'header_view',
                  'content' => 'contact_view',
                  'footer'  =>  'footer_view'
                  );

		$this->load->view('layout/template',$data);
          // $title = $this->input->post('title');
          // $author = $this->input->post('author');
          // $image = $this->input->post('image');
         //  $content = $this->input->post('content');
	}



public function sendemail(){

 $st = $this->db->get('setting')->row();
 $date = $this->input->post('date', TRUE);

 $da   = explode("-",$date);
 $datenew = $da[2].'/'.$da[1].'/'.$da[0];
 $subject = $this->input->post('subject', TRUE);
 $name = $this->input->post('name', TRUE);
 $message = $this->input->post('message', TRUE);
 $address = $this->input->post('address', TRUE);
 $email = $this->input->post('email', TRUE);
 $tel = $this->input->post('tel', TRUE);


	$data2 = array(
			'date' => $datenew,
			'subject' => $subject,
			'name' => $name,
			'detail' => $message,
			'address' => $address,
			'email' => $email,
			'tel' => $tel,
			);





$body = "<div style='margin:0 auto;max-width:450px;width: 100%;padding: 15px;box-sizing: border-box;background:url(https://firebasestorage.googleapis.com/v0/b/ambi-47de7.appspot.com/o/dit%2Flogo.png?alt=media&token=b1341d0c-c623-4ea0-8230-1eeaf939cbf2) no-repeat 50px center #F4A053;background-size:50px auto ;color: #fff;font-family:Arial, sans-serif;font-size:14px;font-weight: bold;text-align: center;'>DIT - แจ้งปัญหา</div>";
$body.= "<div style='max-width:450px;width: 100%;margin:0 auto;'>";
$body.= "<div style='float: left;margin-right: 1.2em;font-size:0.9em;font-weight: bold;color:#323232'>";
$body.= "<style type=\"text/css\">\n";
$body.= ".tg  {border-collapse:collapse;border-spacing:0;width:100%}\n";
$body.= ".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}\n";
$body.= ".tg .tg-nvd3{font-weight:bold;border-color:#ffffff;vertical-align:top}\n";
$body.= ".tg .tg-7wxi{border-color:#ffffff;vertical-align:top}\n";
$body.= "</style>\n";
$body.= "<table class=\"tg\" style='border-collapse:collapse;border-spacing:0;width:100%'>\n";
$body.= "  <tr>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-nvd3\">วันที่</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;:&nbsp;</th>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;".$datenew."</td>\n";
$body.= "  </tr>\n";
$body.= "  <tr>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-nvd3\">ชื่อเรื่อง</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;:&nbsp;</td>\n";
$body.= "    <tdstyle='padding:5px 2px;'  class=\"tg-7wxi\">&nbsp;".$subject."</td>\n";
$body.= "  </tr>\n";
$body.= "  <tr>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">ผู้แจ้ง</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;:&nbsp;</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;".$name."</td>\n";
$body.= "  </tr>\n";
$body.= "  <tr>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">รายละเอียด</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;:&nbsp;</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;".$message."</td>\n";
$body.= "  </tr>\n";
$body.= "  <tr>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">ที่อยู่</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;:&nbsp;</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;".$address."</td>\n";
$body.= "  </tr>\n";
$body.= "  <tr>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">อีเมล</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;:&nbsp;</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;".$email."</td>\n";
$body.= "  </tr>\n";
$body.= "  <tr>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">โทรศัพท์ </td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;:&nbsp;</td>\n";
$body.= "    <td style='padding:5px 2px;' class=\"tg-7wxi\">&nbsp;".$tel."</td>\n";
$body.= "  </tr>\n";
$body.= "</table>";
$body.= "<p></p>";
$body.= "</div>";
$body.= "</div>";


$this->load->library('email');
$config = array(
	'protocol' => 'smtp',
	'smtp_host' => 'ssl://smtp.googlemail.com',
	'smtp_port' => 465,
	'smtp_user' => 'mailer.thinkercorp@gmail.com',
	'smtp_pass' => '00260926',
	'mailtype'  => 'html', 
	'charset'   => 'utf-8'
);
$this->email->initialize($config);
$this->email->set_mailtype("html");
$this->email->set_newline("\r\n");
$this->email->from('info@dit.go.th','DIT WEB');
//$mail_to = array('thunyaporng@gmail.com','gamingkak.017@gmail.com','bowchayanee@gmail.com','gay@thinkerinteractive.com');
$this->email->to($st->mail);

$this->email->subject('แจ้งปัญหา - '.$name.'  #'.date('dmYHis'));//หัวเรื่อง
$this->email->message($body);//ข้อความ
if($this->email->send()):
//echo $this->email->print_debugger();
$this->db->insert('complaint',$data2);
echo "ok";

else:

echo "Can't send mail";

endif;



}


}

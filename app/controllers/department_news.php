<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_news extends CI_Controller {
    function __construct()
    {
     parent::__construct();
     $this->load->model('visit_micro_model');
    }
 
	public function index($id=null,$name=null)
	{
		$this->load->library("pagination");
                 $this->db->where('status !=','close');
                 $this->db->where('micro_id',$id);
		$count = $this->db->get("news_micro")->num_rows();
		$config = array();
        $config["base_url"] = base_url() . "department_news/".$id.'/'.$name;
        $config["total_rows"] = $count;
        $config["per_page"] = 12;
        $config["uri_segment"] = 4;
		$config['full_tag_open'] = '<ul class="ipagination">';
		$config['full_tag_close'] = '</ul>';
		 
		$config['first_link'] = 'หน้าแรก';
		$config['first_tag_open'] = '<li class="firstlink">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'หน้าสุดท้าย';
		$config['last_tag_open'] = '<li class="lastlink">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = '>';
		$config['next_tag_open'] = '<li class="nextlink">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '<';
		$config['prev_tag_open'] = '<li class="prevlink">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="curlink">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li class="numlink">';
		$config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
       // $data["results"] = $this->fetch_news($config["per_page"], $page);
			
        $data = array('content'=>'news_view','id'=>$id,'count'=>$count,'n'=>$this->fetch_data($config["per_page"], $page,$id));
		$this->load->view('layout/template_micro',$data);
	}
	
	public function fetch_data($limit, $start,$id) {
		
		$this->db->limit($limit, $start);
        $this->db->where('status !=','close');
        $this->db->where('micro_id',$id);
		$this->db->order_by('id','desc');
        $query = $this->db->get("news_micro");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
    
    public function detail($id=null,$slug=null,$mi=null)
	{
		if($id == "" || $slug =="" || $mi ==""):
		redirect('/');
		exit();
		endif;
		$this->db->select('id,view');
		$this->db->from('news_micro');
        $this->db->where('id',$id);
        $this->db->where('micro_id',$mi);
		$row=$this->db->get()->row();
		$vi = array(
			'view' => $row->view+1
	     );
	
		 $this->db->where('id', $id);
		 $this->db->update('news_micro', $vi); 
        $data = array('content'=>'news_detail_view','id'=>$id);
		$this->load->view('layout/template_micro',$data);
	}
}

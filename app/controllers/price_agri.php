<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Price_agri extends CI_Controller {

	public function index()
	{
		$this->load->library("pagination");
		$this->db->where('status !=','close');
		$count = $this->db->get("price_agri")->num_rows();
		$config = array();
        $config["base_url"] = base_url() . "price_agri/index";
        $config["total_rows"] = $count;
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
		$config['full_tag_open'] = '<ul class="ipagination">';
		$config['full_tag_close'] = '</ul>';
		 
		$config['first_link'] = 'หน้าแรก';
		$config['first_tag_open'] = '<li class="firstlink">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'หน้าสุดท้าย';
		$config['last_tag_open'] = '<li class="lastlink">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = '>';
		$config['next_tag_open'] = '<li class="nextlink">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '<';
		$config['prev_tag_open'] = '<li class="prevlink">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="curlink">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li class="numlink">';
		$config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data = array('content'=>'price_agri_main_view','count'=>$count,'pp'=>$this->fetch_data($config["per_page"], $page));
		$this->load->view('layout/template',$data);
	}
	public function fetch_data($limit, $start) {
		$this->db->limit($limit, $start);
		$this->db->where('status !=','close');
		$this->db->order_by('id','desc');
        $query = $this->db->get("price_agri");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }



	public function page($id=null,$title=null){

		$this->load->library("pagination");
		$count = $this->db->get_where("price_agri_file",array('id'=>$id))->num_rows();

		if($count!=0):
		$config = array();
        $config["base_url"] = base_url() . "price_agri/page/".$id."/".$title;
        $config["total_rows"] = $count;
        $config["per_page"] = 20;
        $config["uri_segment"] = 5;
		$config['full_tag_open'] = '<ul class="ipagination">';
		$config['full_tag_close'] = '</ul>';
		 
		$config['first_link'] = 'หน้าแรก';
		$config['first_tag_open'] = '<li class="firstlink">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'หน้าสุดท้าย';
		$config['last_tag_open'] = '<li class="lastlink">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = '>';
		$config['next_tag_open'] = '<li class="nextlink">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '<';
		$config['prev_tag_open'] = '<li class="prevlink">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="curlink">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li class="numlink">';
		$config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$data = array('content'=>'price_agri_view','id'=>$id,'count'=>$count,'pp'=>$this->fetch_data_i($config["per_page"], $page,$id));
		else:
			
		$data = array('content'=>'price_agri_view','id'=>$id,'count'=>$count);
        endif;
		$this->load->view('layout/template',$data);

	}


	public function fetch_data_i($limit, $start,$id) {
		$this->db->limit($limit, $start);
		$this->db->order_by('file_id','desc');
		$this->db->where('price_agri_file.id',$id);
        $query = $this->db->get("price_agri_file");
 
        if ($query->num_rows() > 0) {

            return $query->result();
        }
        return false;
   }
}

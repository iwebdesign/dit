<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
        $this->load->library('user_agent');
	}
	public function index()
	{
		if (isset($this->session->userdata['validated'])) {
            redirect('/admin_dit');
          }
            $this->load->view('login/template');

	}


	  private function _validate(){
        $this->load->library('encrypt');
 
        
        // grab user input
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));
        
        $u = $this->db->get_where('users',array('username'=>$username))->row();
        
        if($u!=false):
        $key = 'super-secret-key';
        $pass = $this->encrypt->decode($u->password, $key);

        // Prep the query
        // $this->db->where('username', $username);
        // $this->db->where('password',  $this->encrypt->encode($u->password, $key));
        
        // Run the query
       // $query = $this->db->get('users');
        // Let's check if there are any results
        if($password == $pass)
        {
            // If there is a user, then create session data
           // $row = $query->row();
            $data = array(
                    // 'userid' => $row->userid,
                    'username' => $username,
                    'validated' => true
                    );
            $this->session->set_userdata($data);

            $da   = array(
                         'username'=>$username,
                         'user_agent' => $this->input->user_agent(),
                         'ip'      => $this->input->ip_address(),
                         'date_time'=> date('d/m/Y H:i:s')
                         );
            
            $this->db->insert('user_log',$da);
            return true;
		}
        return false;
    endif;

        // If the previous process did not validate
        // then return false.
        return false;
    }

    public function checkuser(){
        if($this->_validate()==true):
            redirect('/admin_dit');
        else:
            $data=array('st'=>'username หรือ password ไม่ถูกต้อง');
            $this->load->view('login/template',$data);
        endif;
    }

    public function logout() {

		// Removing session data
		$sess_array = array(
			'userid' => '',
			'username' => '',
			'validated' => ''
			);
		$this->session->unset_userdata($sess_array);
		$this->session->sess_destroy();
		redirect('login');
		}
}
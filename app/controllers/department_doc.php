<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_doc extends CI_Controller {
    function __construct()
    {
     parent::__construct();
     $this->load->model('visit_micro_model');
    }
 
	public function index($id=null,$name=null,$idfile=null)
	{
		$this->load->library("pagination");
                 $this->db->where('id',$idfile);
		$count = $this->db->get("doc_micro_file")->num_rows();
		$config = array();
        $config["base_url"] = base_url() . "department_doc/".$id."/".$name."/".$idfile;
        $config["total_rows"] = $count;
        $config["per_page"] = 20;
        $config["uri_segment"] = 5;
		$config['full_tag_open'] = '<ul class="ipagination">';
		$config['full_tag_close'] = '</ul>';
		 
		$config['first_link'] = 'หน้าแรก';
		$config['first_tag_open'] = '<li class="firstlink">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'หน้าสุดท้าย';
		$config['last_tag_open'] = '<li class="lastlink">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = '>';
		$config['next_tag_open'] = '<li class="nextlink">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '<';
		$config['prev_tag_open'] = '<li class="prevlink">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="curlink">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li class="numlink">';
		$config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data = array('content'=>'doc_main_view','id'=>$id,'idfile'=>$idfile,'count'=>$count,'pp'=>$this->fetch_data($config["per_page"], $page,$idfile));
		$this->load->view('layout/template_micro',$data);
	}
	public function fetch_data($limit, $start,$idfile) {
		$this->db->limit($limit, $start);
		$this->db->where('id',$idfile);
		$this->db->order_by('file_id','desc');
        $query = $this->db->get("doc_micro_file");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

}
